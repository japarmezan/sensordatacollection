﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DataGenerator.Utils;
using DotNet.Highcharts;
using Newtonsoft.Json;

namespace DataVisualizator.Models
{
    public class TemperatureViewModel
    {
        [Required(ErrorMessage = "select one item")]
        public FuncType FuncType { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int MinTemperature { get; set; }
        public int MaxTemperature { get; set; }
        public int StepInSeconds { get; set; }
        public Highcharts Chart { get; set; }
        public Highcharts ChartFiltered { get; set; }
        public string Response { get; set; }

        public int from { get; set; }
    }
}