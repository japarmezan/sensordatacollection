﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataVisualizator.Models
{
    public class TimePoint
    {
        public int TimeStamp { get; set; }
        public int Value { get; set; }

    }
}