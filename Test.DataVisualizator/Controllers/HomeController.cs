﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cloud.Services.DataEntities.SensorEntry;
using Core.StreamDataProcessor.Filter;
using Core.StreamDataProcessor.Processor;
using DataGenerator;
using DataVisualizator.Models;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using Newtonsoft.Json;

namespace DataVisualizator.Controllers
{
    public class HomeController : Controller
    {
        private IDataGenerator _dataGenerator;

        private IStreamProcessor _streamProcessor;

        private IStreamFilter _streamFilter;
        
        private IList<SenzorEntryModel> _processedData;
        


        [HttpGet]
        public ActionResult Index()
        {
            TemperatureViewModel model = new TemperatureViewModel
            {
                StepInSeconds = 1000,
                MaxTemperature = 20,
                EndTime = 86400
            };
            
            return View("Index", model);
        }

        [HttpPost]
        public ViewResult Generate(TemperatureViewModel model)
        {
            _dataGenerator = new DataGenerator.DataGenerator();

            //var data =_dataGenerator.GenerateTemperatureDataSet(model.StepInSeconds, model.FuncType, model.MinTemperature,
            //    model.MaxTemperature, model.StartTime, model.EndTime);

            //var data = _dataGenerator.GeneratePresenceSensorDataSet(1, 100);

            var data = _dataGenerator.ImportDataset(@"C:\\Users\\janim\\Documents\\TapHome\\testovacie_logy\\TapHome.DeviceValueLogs.2015-08-22_17-28-22(2).tsv");


            var timeData = data.Select(x => new object[] { new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local).AddSeconds(x.Key), Math.Round(Convert.ToDouble(x.Value), 2) }).ToArray();


            model.Chart = new Highcharts("chart")
                .InitChart(new Chart {DefaultSeriesType = ChartTypes.Spline, ZoomType = ZoomTypes.X})
                .SetTitle(new Title {Text = "Temperature "})
                .SetXAxis(new XAxis
                {
                    Type = AxisTypes.Datetime,
                    DateTimeLabelFormats = new DateTimeLabel
                    {
                        Second = "%H:%M:%S",
                        Minute = "%H:%M",
                        Hour = "%H:%M"
                    }
                })
                .SetTooltip(new Tooltip
                {
                    Enabled = true, PointFormat = "{point.x:%H:%M:%S} => {point.y}"
                    //Formatter = @"function() { return 'Temperature at '+ this.x +' => ' + this.y; } "
                })
                .SetNavigation(new Navigation {ButtonOptions = new ExportingButtonsExportButton()})
                .SetSeries(new[]
                {
                    new Series {Name = "Temp", Data = new Data(timeData)}
                });

           
            // initialize stream processing
            _streamFilter = new StreamFilter();
            _streamProcessor = new StreamProcessor(_streamFilter);

            FilterSettings filterSettings = new FilterSettings(FilterType.AnalogDataFilter, 10);

            // process raw data to SenzorEntries
            var location = Guid.NewGuid();
            var session = Guid.NewGuid();
            var deviceId = 1112;
            var valueTypeId = 1;
            var chartValueDesc = new ChartValueDescription
            {
                EntryValueType = EntryValueType.Analog,
                ChartType = ChartType.LineChart
            };

            _processedData = (List<SenzorEntryModel>)_streamProcessor.BatchProcessData(data, session, location, deviceId, valueTypeId, chartValueDesc, filterSettings);


            // draw chart
            var ptimeData = _processedData.Select(x => new object[] { new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local).AddSeconds(x.TimeStamp), Math.Round(Convert.ToDouble(x.Value), 2) }).ToArray();
            
            model.ChartFiltered = new Highcharts("chartFiltered")
                .InitChart(new Chart { DefaultSeriesType = ChartTypes.Spline, ZoomType = ZoomTypes.X })
                .SetTitle(new Title { Text = "Filtered Temperature" })
                .SetXAxis(new XAxis { Type = AxisTypes.Datetime, DateTimeLabelFormats = new DateTimeLabel
                    {
                        Second = "%H:%M:%S", Minute = "%H:%M", Hour = "%H:%M"
                    }
                })
                .SetTooltip(new Tooltip
                {
                    Enabled = true, PointFormat = "{point.x:%H:%M:%S} => {point.y}"
                    //Formatter = @"function() { return 'Temperature at '+ this.x +' => ' + this.y; } "
                })
                .SetNavigation(new Navigation { ButtonOptions = new ExportingButtonsExportButton() })
                .SetSeries(new[]
                {
                        new Series {Name = "Temp", Data = new Data(ptimeData)/*, PlotOptionsLine = new PlotOptionsLine {Step = "true"} */}
                });


            //model.Response = await PostData(_processedData);
            
            return View("Index", model);
        }


        private async Task<string> PostData(IList<SenzorEntryModel> data)
        {
            using (var client = new HttpClient())
            {
                var response = client.PostAsync("http://localhost:5206/api/senzorentry",
                    new StringContent(JsonConvert.SerializeObject(data),Encoding.UTF8, "application/json")).Result;
                
                return await response.Content.ReadAsStringAsync();
            }
        }

        [HttpGet]
        public async Task<ActionResult> GetRange()
        {
            return View("Index", new TemperatureViewModel
            {
                Response = await GetRangeData()
            });







        }

        private async Task<string> GetRangeData()
        {
            using (var client = new HttpClient())
            {
                var url =
                    $"http://localhost:5206/api/senzorentry/?locationId={"Test"}&deviceId={1111}&valueTypeId={1}&from={9028773}&to={9043773}&granularity={"d"}";
                var response = client.GetAsync(url).Result;

                return await response.Content.ReadAsStringAsync();
            }
        }


        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return View("Index", new TemperatureViewModel
            {
                Response = await GetAllData()
            });
        }

        private async Task<string> GetAllData()
        {
            using (var client = new HttpClient())
            {
                var response = client.GetAsync("http://localhost:5206/api/senzorentry").Result;

                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}