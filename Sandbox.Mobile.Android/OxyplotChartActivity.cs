using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Xamarin.Android;
using System.Security.Cryptography;
using System.Text;
using Android.Widget;
using Mobile.Chart.Droid;
using Mobile.Chart.Droid.OxyplotChart;
using Mobile.Chart.Droid.SeriesDefinitions;
using OxyPlot.Axes;
using Debug = System.Diagnostics.Debug;

namespace Sandbox.Mobile.Android
{
    [Activity(Label = "Oxyplot chart prototype", MainLauncher = true, Icon = "@drawable/icon")]
    public class OxyplotChartActivity : Activity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);
            FrameLayout layout = (FrameLayout) FindViewById(Resource.Id.samplearea);


            var chart = new OxyplotChartRenderer(this)
            {
                LoadTimespansDefinition = {CoreValueLoadTimeSpan = new TimeSpan(1, 0, 0)}
            };


            chart.AddSeries(new SensorInfo("Test", "1111", "1", ValueFunction.Average), new List<string> {"v", "b"});

            chart.Render(layout);

        }
    }
}