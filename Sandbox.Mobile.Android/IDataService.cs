using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cloud.Services.DataEntities.SensorEntry;

namespace Sandbox.Mobile.Android
{
    interface IDataService
    {
        Task<IList<SenzorEntryModel>> GetRange(string locationId, string deviceId, string valueTypeId, int from, int to,
            string granularity);
    }
}