using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Sandbox.Mobile.Android
{
    [Activity(Label = "TelerikChartActivity", MainLauncher = false)]
    public class TelerikChartActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);
            // Create your application here

        //    RadCartesianChartView chartView = new RadCartesianChartView(this);

        //    ViewGroup rootView = (ViewGroup)FindViewById(Resource.Id.samplearea);

        //    InitData();

        //    LineSeries lineSeries = new LineSeries();
        //    lineSeries.CategoryBinding = new MonthResultDataBinding("Month");
        //    lineSeries.ValueBinding = new MonthResultDataBinding("Result");
        //    lineSeries.Data = (Java.Lang.IIterable)this.monthResults;
        //    chartView.Series.Add(lineSeries);

        //    CategoricalAxis horizontalAxis = new CategoricalAxis();
        //    chartView.HorizontalAxis = horizontalAxis;

        //    LinearAxis verticalAxis = new LinearAxis();
        //    chartView.VerticalAxis = verticalAxis;

        //    rootView.AddView(chartView);
        //}

        //private Java.Util.ArrayList monthResults;

        //private void InitData()
        //{
        //    monthResults = new Java.Util.ArrayList();
        //    monthResults.Add(new MonthResult("Jan", 12));
        //    monthResults.Add(new MonthResult("Feb", 5));
        //    monthResults.Add(new MonthResult("Mar", 10));
        //    monthResults.Add(new MonthResult("Apr", 7));
        }
    }
    //public class MonthResult : Java.Lang.Object
    //{

    //    public String Month { get; set; }
    //    public double Result { get; set; }

    //    public MonthResult(String month, double result)
    //    {
    //        this.Month = month;
    //        this.Result = result;
    //    }
    //}

    //class MonthResultDataBinding : DataPointBinding
    //{

    //    private string propertyName;

    //    public MonthResultDataBinding(string propertyName)
    //    {
    //        this.propertyName = propertyName;
    //    }

    //    public override Java.Lang.Object GetValue(Java.Lang.Object p0)
    //    {
    //        if (propertyName == "Month")
    //        {
    //            return ((MonthResult)(p0)).Month;
    //        }
    //        return ((MonthResult)(p0)).Result;
    //    }
    //}
}