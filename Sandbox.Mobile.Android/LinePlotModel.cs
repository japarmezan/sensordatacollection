﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Android.Content;
using Android.Widget;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Core.StreamDataProcessor.Filter;
using DataGenerator.Utils;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;

namespace Sandbox.Mobile.Android
{
    public class LinePlotModel
    {

        public LinePlotModel(IList<SenzorEntryModel> data)
        {

            this.Model = new PlotModel()
            {
                PlotAreaBorderThickness = new OxyThickness(0),
                PlotAreaBorderColor = OxyColors.Transparent
            };
            
            Model.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.None,
                MinorGridlineStyle = LineStyle.None,
                TickStyle = TickStyle.Crossing,
                IsZoomEnabled = false,
                IsPanEnabled = false,
                IsAxisVisible = true,
                AxislineColor = OxyColors.Black
            });

            var xAxis = new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                IntervalType = DateTimeIntervalType.Auto,
                MinorIntervalType = DateTimeIntervalType.Auto,
                IsZoomEnabled = true,
                IsPanEnabled = true,
                //AbsoluteMinimum = DateTimeAxis.ToDouble(data.First().TimeStamp),
                //AbsoluteMaximum = DateTimeAxis.ToDouble(data.Last().TimeStamp),
                TickStyle = TickStyle.None,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineColor = OxyColors.DimGray,
                MajorGridlineThickness = 1,
                MinorGridlineStyle = LineStyle.Dot,
                MinorGridlineColor = OxyColors.DimGray,
                MinorGridlineThickness = 0.5
            };

            Model.Axes.Add(xAxis);
            
            UpdateSeries(data);

            Model.SelectionColor = OxyColors.Red;
            //l.TrackerFormatString = "{2}: {4}";
        }


        public PlotModel Model { get; set; }

        public void UpdateSeries(IList<SenzorEntryModel> data)
        {
            Model.Series.Clear();

            var oxyplotData =
                data.Where(x => !double.IsNaN(x.Value))
                    .Select(
                        x => DateTimeAxis.CreateDataPoint(TapHomeTime.FromTapHomeTime(x.TimeStamp), Math.Round(x.Value, 2)))
                    .ToList();


            Model.Axes[1].AbsoluteMinimum = oxyplotData.First().X;
            Model.Axes[1].AbsoluteMaximum = oxyplotData.Last().X;

            var l = new LineSeries
            {
                Color = OxyColors.Blue,
                MarkerType = MarkerType.Circle,
                MarkerSize = 2,
                MarkerFill = OxyColors.Black,
                MarkerStrokeThickness = 0.5,
                MarkerStroke = OxyColors.WhiteSmoke,
                SelectionMode = SelectionMode.Single
            };


            l.Points.AddRange(oxyplotData);
            l.ItemsSource = oxyplotData;

            Model.Series.Add(l);
            Model.InvalidatePlot(true);
        }

    }
}
