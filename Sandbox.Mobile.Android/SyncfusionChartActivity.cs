﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Com.Syncfusion.Charts.Enums;
using DataGenerator.Utils;
using Java.Util;
using Mobile.Chart.Droid;
using Mobile.Chart.Droid.SeriesDefinitions;
using IChartRenderer = Com.Syncfusion.Charts.IChartRenderer;
using Random = System.Random;

namespace Sandbox.Mobile.Android
{
    [Activity(Label = "Syncfusion chart prototype", MainLauncher = false, Icon = "@drawable/icon")]
    public class SyncfusionChartActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            SetContentView(Resource.Layout.Main);
            FrameLayout layout = (FrameLayout) FindViewById(Resource.Id.samplearea);
            

            //var chart = new SyncfusionChartRenderer(this);

            //chart.AddSeries(new SensorInfo("Test", "1111", "1", ValueFunction.Average), new List<string> { "v", "b" });

            //chart.Render(layout);
        }

    }
}
