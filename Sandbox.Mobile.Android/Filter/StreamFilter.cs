﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloud.Services.DataEntities.SensorEntry;

namespace Core.StreamDataProcessor.Filter
{
    public class StreamFilter : IStreamFilter
    {

        public IList<SenzorEntryModel> FilterAnalogData(IList<SenzorEntryModel> data, FilterSettings filterSettings)
        {
            if (filterSettings.MinChangeToDetect > 100 || filterSettings.MinChangeToDetect < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(data), "FilterSettings: MinChangeToDetect must be a percentage value from 0 to 100.");
            }

            // find minimum and maximum and count minimal change to detect
            var max = data.First().Value;
            var min  = data.First().Value;

            min = data.Select(entry => entry.Value).Concat(new[] { min }).Min();
            max = data.Select(entry => entry.Value).Concat(new[] { max }).Max();
            
            var minChange = (max - min) * (filterSettings.MinChangeToDetect / 100);

            // take first value
            IList<SenzorEntryModel> filteredData = new List<SenzorEntryModel>();
            filteredData.Add(data.First());

            // loop the batch
            foreach (var entry in data.Skip(1))
            {
                // compare change with last filtered value and save only if the change is significant
                var lastReferenceValue = filteredData.Last().Value;
                if (entry.Value > lastReferenceValue + minChange || entry.Value < lastReferenceValue - minChange)
                {
                    filteredData.Add(entry);
                }
            }

            return filteredData;
        }

        public IDictionary<int, float> FilterAnalogData(IDictionary<int, float> data, FilterSettings filterSettings)
        {
            if (filterSettings.MinChangeToDetect > 100 || filterSettings.MinChangeToDetect < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(data), "FilterSettings: MinChangeToDetect must be a percentage value from 0 to 100.");
            }

            // find minimum and maximum and count minimal change to detect
            var max = data.First().Value;
            var min = data.First().Value;

            min = data.Select(entry => entry.Value).Concat(new[] { min }).Min();
            max = data.Select(entry => entry.Value).Concat(new[] { max }).Max();

            var minChange = (max - min) * (filterSettings.MinChangeToDetect / 100);

            // take first value
            IDictionary<int, float> filteredData = new Dictionary<int, float>();
            filteredData.Add(data.First());

            // loop the batch
            foreach (var entry in data.Skip(1))
            {
                // compare change with last filtered value and save only if the change is significant
                var lastReferenceValue = filteredData.Last().Value;
                if (entry.Value > lastReferenceValue + minChange || entry.Value < lastReferenceValue - minChange)
                {
                    filteredData.Add(entry);
                }
            }

            return filteredData;
        }


        public IList<SenzorEntryModel> FilterDigitalData(IList<SenzorEntryModel> data)
        {
            
            IList<SenzorEntryModel> filteredData = new List<SenzorEntryModel>();

            // take first value from batch and save value and sessionId
            filteredData.Add(data.First());
            var value = data.First().Value;
            var session = data.First().SessionId;

            // loop the batch 
            foreach (var entry in data.Skip(1))
            {
                // if value changed from 0 to 1, or session changed, take the value
                if (entry.Value - value > 0.5 || !entry.SessionId.Equals(session))
                {
                    filteredData.Add(entry);
                }

                // remember new value and sessionId
                value = entry.Value;
                session = entry.SessionId;
            }

            return filteredData;
        }

        // obsolete
        private class TimestampComparer : IComparer<SenzorEntryModel>
        {
            public int Compare(SenzorEntryModel x, SenzorEntryModel y)
            {
                if (x == null)
                {
                    if (y == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return y == null ? 1 : x.TimeStamp.CompareTo(y.TimeStamp);
                }
            }
        }
    }
}
