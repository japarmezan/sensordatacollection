﻿using System.Collections.Generic;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Storage.Repository;

namespace Cloud.Services.Controllers.Services
{
    interface IMergeService
    {
        IList<SenzorEntryModel> MergeBatch(IList<SenzorEntryModel> entries, MergeFunctionEnum functions);
        IList<SenzorEntryModel> GetAndMergeRegularIntervals(IList<SenzorEntryModel> entries, MergeFunctionEnum functions, IStorageRepository repository);
    }
}
