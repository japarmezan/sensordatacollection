﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Cloud.Services.Models.Mappers;
using Cloud.Services.MoreLinq;
using Cloud.Storage.Entities;
using Cloud.Storage.Repository;

namespace Cloud.Services.Controllers.Services
{
    public class MergeService : IMergeService
    {
        public IList<SenzorEntryModel> MergeBatch(IList<SenzorEntryModel> entries, MergeFunctionEnum functions)
        {
            var entry = entries.First(); // TODO: rethink session id for new entry

            var mergedEntries = new List<SenzorEntryModel>();
            
            if ((functions & MergeFunctionEnum.Average) == MergeFunctionEnum.Average)
            {
                // TODO: sessions should not be taken into account, or do they?
                //var sessions = entries.GroupBy(x => x.SessionId);
                //foreach (var session in sessions)
                //{
                //    if (session.Count() > 1)
                //    }
                //}
                
                var sum = entries.Sum(x => x.Value);
                var count = entries.Count;
                var average = sum / count;
                var avgTimestamp = (entries.First().TimeStamp + entries.Last().TimeStamp)/2;

                mergedEntries.Add(new SenzorEntryModel(entry, avgTimestamp, average, Granularity.CoreHourBatch, ValueFunction.Average));
            }

            if ((functions & MergeFunctionEnum.Minimum) == MergeFunctionEnum.Minimum)
            {
                var min = entries.MinBy(x => x.Value);

                mergedEntries.Add(new SenzorEntryModel(entry, min.TimeStamp, min.Value, Granularity.CoreHourBatch, ValueFunction.Minimum));
            }

            if ((functions & MergeFunctionEnum.Maximum) == MergeFunctionEnum.Maximum)
            {
                var max = entries.MaxBy(x => x.Value);

                mergedEntries.Add(new SenzorEntryModel(entry, max.TimeStamp, max.Value, Granularity.CoreHourBatch, ValueFunction.Maximum));
            }
            
            return mergedEntries;
        }

        public IList<SenzorEntryModel> GetAndMergeRegularIntervals(IList<SenzorEntryModel> entries, MergeFunctionEnum functions, IStorageRepository repository)
        {
            var nullEntries = entries.Where(x => x.Value.Equals(double.NaN)).ToList();

            if (nullEntries.Count.Equals(0))
            {
                return (List<SenzorEntryModel>) entries;
            }

            var lowerGranularity = Granularity.GetLowerLevel(entries.First().Granularity);


            // Get missing ranges
            var fromTaphomeTimes = new List<int>();
            var toTaphomeTimes = new List<int>();

            foreach (var entry in nullEntries)
            {
                var from = entry.TimeStamp - 60;
                fromTaphomeTimes.Add(from);

                switch (lowerGranularity)
                {
                    case Granularity.DayRegular:
                        toTaphomeTimes.Add(TapHomeTime.ToTapHomeTime(TapHomeTime.FromTapHomeTime(from).AddDays(1)) + 60);
                        break;
                    case Granularity.WeekRegular:
                        toTaphomeTimes.Add(TapHomeTime.ToTapHomeTime(TapHomeTime.FromTapHomeTime(from).AddDays(7)) + 60);
                        break;
                    case Granularity.MonthRegular:
                        toTaphomeTimes.Add(TapHomeTime.ToTapHomeTime(TapHomeTime.FromTapHomeTime(from).AddMonths(1)) + 60);
                        break;
                    case Granularity.YearRegular:
                        toTaphomeTimes.Add(TapHomeTime.ToTapHomeTime(TapHomeTime.FromTapHomeTime(from).AddYears(1)) + 60);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            var firstEntry = entries.First();
            var entities = repository.GetRanges(firstEntry.LocationId.ToString(), firstEntry.DeviceId.ToString(), firstEntry.ValueTypeId.ToString(), fromTaphomeTimes,
                toTaphomeTimes, lowerGranularity, ValueFunction.Average);

            var models = TableEntityMapper.MapTableEntitiesToModels((IEnumerable<SenzorEntryEntity>) entities, 0, 0, "", false);

            // Merge entries
            var mergedModels = MergeBatch(models, functions);

            // Store missing entries to tablestorage
            repository.Insert(TableEntityMapper.MapModelsToTableEntities(mergedModels), firstEntry.LocationId.ToString());

            // Put missing entries to original list instead of NaN values
            var returnModels = (List<SenzorEntryModel>) entries;
            returnModels.RemoveAll(x => x.Value.Equals(double.NaN));
            returnModels.AddRange(mergedModels);


            return returnModels;
        }


    }
}