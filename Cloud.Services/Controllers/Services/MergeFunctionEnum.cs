﻿using System;

namespace Cloud.Services.Controllers.Services
{
    [Flags]
    public enum MergeFunctionEnum
    {
        Average,
        Minimum,
        Maximum
    }
}