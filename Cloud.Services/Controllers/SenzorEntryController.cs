﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cloud.Services.Controllers.Services;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.Models.Mappers;
using Cloud.Storage.Entities;
using Cloud.Storage.Repository;

namespace Cloud.Services.Controllers
{
    public class SenzorEntryController : ApiController
    {
        private readonly IMergeService _mergeService;

        private readonly IStorageRepository _repository;


        public SenzorEntryController()
        {
            // TODO: Dependency Injection
            _mergeService = new MergeService();
            _repository = new TableStorageRepository();
        }

        [HttpPost]
        [Route("api/sensorentries")]
        // POST api/sensorentries
        public HttpResponseMessage Insert(List<SenzorEntryModel> entries)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();

            if (entries.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error: Sequence contains no entries.");
            }

            var mergedEntries = _mergeService.MergeBatch(entries, MergeFunctionEnum.Average);
            entries.AddRange(mergedEntries);
            
            do
            {
                IList<SenzorEntryModel> batch = entries.Take(100).ToList();

                var tableEntities = TableEntityMapper.MapModelsToTableEntities(batch);
                var tableName = entries.First().LocationId.ToString();

                _repository.Insert(tableEntities, "Test");
                
                var removeItemsCount = entries.Count >= 100 ? 100 : entries.Count;  
                entries.RemoveRange(0, removeItemsCount);

            } while (entries.Count > 0);

            timer.Stop();
            Debug.WriteLine("Insert: " + timer.ElapsedMilliseconds + " ms");
            
            return Request.CreateResponse(HttpStatusCode.OK, "Entries successfully added.");
        }

        [HttpGet]
        [Route("api/sensorentries")]
        // GET api/sensorentries
        public HttpResponseMessage GetRange(string locationId, string deviceId, int valueTypeId, int from, int to, string granularity, string valueFunction)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();

            if (from > to)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error: Start of the range is later in time then the end.");
            }

            if (valueTypeId < 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error: Invalid valueTypeId: " + valueTypeId);
            }

            if (!Granularity.IsValidCode(granularity))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error: Invalid granularity code: " + granularity + ". Possible options are: " + Granularity.ToString());
            }

            if (!ValueFunction.IsValidCode(valueFunction))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error: Invalid valueFunction code: " + granularity + ". Possible options are: " + ValueFunction.ToString());
            }

            // call table storage Get
            var entities = _repository.GetRange(locationId, deviceId, valueTypeId, from, to, granularity, valueFunction);


            // map to models and add NaN values to empty spaces in interval
            var models = TableEntityMapper.MapTableEntitiesToModels((IEnumerable<SenzorEntryEntity>) entities, from, to, granularity, false);

            // download month granularity if missing - not tested yet
            //if (granularity == Granularity.MonthRegular)
            //{
            //    if (models.Any(x => x.Value.Equals(double.NaN)))
            //    {
            //        models = _mergeService.GetAndMergeRegularIntervals(models, MergeFunctionEnum.Average, _repository);
            //    }
            //}
            
            timer.Stop();
            Debug.WriteLine("GetRange: " + timer.ElapsedMilliseconds + " ms");

            return Request.CreateResponse(HttpStatusCode.OK, models);
        }

        //public IEnumerable<SenzorEntryModel> GetAll(string locationId)
        //{
        //    var models = TableEntityMapper.MapTableEntitiesToModels((IEnumerable<SenzorEntryEntity>)_repository.GetAll("Test"), 0, 0, null, false);

        //    return models;
        //}


        // DELETE api/deletelocation
        [HttpDelete]
        [Route("api/deletelocation")]
        public HttpResponseMessage DeleteLocation(string locationId)
        {
            if (_repository.DeleteTable(locationId))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Location successfully deleted.");
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error: Location" + locationId + " could not be deleted.");
        }

        [HttpGet]
        [Route("api/signature")]
        // GET api/signature
        public HttpResponseMessage GetSasSignature(string locationId)
        {
            var sasString = _repository.GetQueryPermissionSas(locationId);

            if (sasString == null)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "SAS signature could not be delivered.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, sasString);
        }

        [HttpDelete]
        [Route("api/signature")]
        // GET api/signature
        public HttpResponseMessage RemoveSasSignature(string locationId)
        {
            var removed = _repository.RemoveQueryPermissionSas(locationId);

            if (!removed)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "SAS signature with locationId " + locationId + " could not be removed.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, "SAS signature was successfully removed.");
        }

    }
}
