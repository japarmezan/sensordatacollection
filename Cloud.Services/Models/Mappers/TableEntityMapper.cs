﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Cloud.Storage.Entities;
using DateTimeExtensions;

namespace Cloud.Services.Models.Mappers
{
    public static class TableEntityMapper
    {
        public static IList<SenzorEntryEntity> MapModelsToTableEntities(IList<SenzorEntryModel> models)
        {
            return (from model in models
                let partitionKey = model.DeviceId + "_" + model.Granularity
                let rowKey = model.ValueFunction + "_" + model.ValueTypeId + "_" + model.TimeStamp.ToString("D10")
                select new SenzorEntryEntity(partitionKey, rowKey)
                {
                    //Value = model.Value.ToString("0.00"),
                    //SessionId = model.SessionId.ToString()
                    Value = model.Value,
                    SessionId = model.SessionId
                }).ToList();
        }

        public static IList<SenzorEntryModel> MapTableEntitiesToModels(IEnumerable<SenzorEntryEntity> entities, int from, int to, string granularity, bool insertMissingEntries)
        {
            var models = (from entity in entities
                    let deviceId = entity.PartitionKey.Split('_')[0]
                    let granularityValue = entity.PartitionKey.Split('_')[1]
                    let valueFunction = entity.RowKey.Split('_')[0]
                    let valueTypeId = entity.RowKey.Split('_')[1]
                    let timestamp = entity.RowKey.Split('_')[2]
                    select new SenzorEntryModel
                {
                    SessionId = entity.SessionId,
                    DeviceId = Convert.ToInt64(deviceId),
                    TimeStamp = Convert.ToInt32(timestamp),
                    Granularity = granularityValue,
                    Value = Convert.ToDouble(entity.Value),
                    ChartValueDescription = new ChartValueDescription(),
                    ValueTypeId = Convert.ToInt32(valueTypeId),
                    ValueFunction = valueFunction
                }).ToList();


            if (insertMissingEntries && from != 0 && to != 0 && granularity != null)
            {
                models = InsertMissingEntries(models, from, to, granularity);
            }

            return models;
        }

        private static List<SenzorEntryModel> InsertMissingEntries(List<SenzorEntryModel> models, int from, int to, string granularity)
        {
            var leadingTimestamps = new List<int>();
            var modelTimestamps = models.Select(x => x.TimeStamp);

            switch (granularity)
            {
                case Granularity.YearRegular:

                    var fromYear = TapHomeTime.FromTapHomeTime(from).Year;
                    var fromDateY = new DateTime(fromYear, 1, 1).SetTime(0, 0, 0);

                    var toYear = TapHomeTime.FromTapHomeTime(to).Year;
                    var toDateY = new DateTime(toYear, 1, 1).SetTime(0, 0, 0);

                    while (fromDateY <= toDateY)
                    {
                        leadingTimestamps.Add(TapHomeTime.ToTapHomeTime(fromDateY));
                        fromDateY = fromDateY.AddYears(1);
                    }


                    break;

                case Granularity.MonthRegular:
                    var fromDate = TapHomeTime.FromTapHomeTime(from);
                    fromDate = new DateTime(fromDate.Year, fromDate.Month, 1).SetTime(0, 0, 0).AddMonths(1);
                    var toDate = TapHomeTime.FromTapHomeTime(to);
                    toDate = new DateTime(toDate.Year, toDate.Month, 1).SetTime(0, 0, 0);

                    while (fromDate <= toDate)
                    {
                        leadingTimestamps.Add(TapHomeTime.ToTapHomeTime(fromDate));
                        fromDate = fromDate.AddMonths(1);
                    }


                    break;

                case Granularity.WeekRegular:
                    var fromDateW = TapHomeTime.FromTapHomeTime(from);
                    int delta = DayOfWeek.Monday - fromDateW.DayOfWeek;
                    fromDateW = fromDateW.AddDays(delta).SetTime(0, 0, 0).AddDays(7);

                    var toDateW = TapHomeTime.FromTapHomeTime(to);
                    delta = DayOfWeek.Monday - toDateW.DayOfWeek;
                    toDateW = toDateW.AddDays(delta).SetTime(0, 0, 0);

                    while (fromDateW <= toDateW)
                    {
                        leadingTimestamps.Add(TapHomeTime.ToTapHomeTime(fromDateW));
                        fromDateW = fromDateW.AddDays(7);
                    }

                    break;

                case Granularity.DayRegular:
                    var fromDateD = TapHomeTime.FromTapHomeTime(from).SetTime(0, 0, 0).AddDays(1);

                    var toDateD = TapHomeTime.FromTapHomeTime(to).SetTime(0, 0, 0);

                    while (fromDateD <= toDateD)
                    {
                        leadingTimestamps.Add(TapHomeTime.ToTapHomeTime(fromDateD));
                        fromDateD = fromDateD.AddDays(1);
                    }

                    break;

                default:
                    return models;
            }

            // find all missing timestamps
            var differenceTimestamps = leadingTimestamps.Except(modelTimestamps);
            var model = models.First();

            // add them to models with NaN value
            models.AddRange(differenceTimestamps.Select(timestamp => new SenzorEntryModel(model, timestamp, double.NaN, granularity, ValueFunction.Average)));

            return models;
        }
    }
}
