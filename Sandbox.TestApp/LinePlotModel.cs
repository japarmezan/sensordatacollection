﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot.Axes;
using Sandbox.TestApp.Utils;

namespace Sandbox.TestApp
{
    using System;

    using OxyPlot;
    using OxyPlot.Series;

    public class LinePlotModel
    {
        public LinePlotModel(IDictionary<int, float> data)
        {
            this.Model = new PlotModel();
            var oxyplotData = data.Where(x => !double.IsNaN(x.Value)).Select(x => DateTimeAxis.CreateDataPoint(TapHomeTime.FromTapHomeTime(x.Key), Math.Round(x.Value, 2))).ToList();

            Model.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, TickStyle = TickStyle.Outside
            });
            
            Model.Axes.Add(new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                StringFormat = "HH:mm",
                IntervalType = DateTimeIntervalType.Auto,
                MinorIntervalType = DateTimeIntervalType.Auto
            });


            var l = new LineSeries
            {
                Color = OxyColors.SkyBlue,
                MarkerType = MarkerType.Circle,
                MarkerSize = 3,
                MarkerFill = OxyColors.Blue,
                MarkerStrokeThickness = 1.5,
                Smooth = false,
                Selectable = true,
                SelectionMode = SelectionMode.Single
            };
            l.Points.AddRange(oxyplotData);
            Model.Series.Add(l);

            Model.SelectionColor = OxyColors.Red;
            //l.TrackerFormatString = "{2}: {4}";
        }

        public PlotModel Model { get; private set; }
    }
}
