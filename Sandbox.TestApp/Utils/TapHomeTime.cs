﻿using System;

namespace Sandbox.TestApp.Utils
{

    public class TapHomeTime
    {
        public static DateTime TAP_HOME_EPOCH = new DateTime(2016, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        
        public static int TapHomeNow()
        {
            return (int) (DateTime.Now - TAP_HOME_EPOCH).TotalSeconds;
        }

        public static int ToTapHomeTime(DateTime fromTime)
        {
            return (int)(fromTime - TAP_HOME_EPOCH).TotalSeconds;
        }

        public static DateTime FromTapHomeTime(int fromTime)
        {
            return TAP_HOME_EPOCH.AddSeconds(fromTime);
        }

    }
}
