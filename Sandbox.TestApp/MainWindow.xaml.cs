﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Core.StreamDataProcessor.Filter;
using DataGenerator;
using Sandbox.TestApp.Utils;
using Cloud.Services.DataEntities.SensorEntry;
using Microsoft.Win32;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using FuncType = DataGenerator.Utils.FuncType;


namespace Sandbox.TestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IDataGenerator _dataGenerator;
        private IStreamFilter _streamFilter;
        private IDictionary<int, float> _data;
        private IDictionary<int, float> _filteredData;

        private string apiUrl = "http://localhost:5206";
        //private string apiUrl = "http://taphomecharttest.azurewebsites.net";

        public MainWindow()
        {
            InitializeComponent();

            _dataGenerator = new DataGenerator.DataGenerator();

            _streamFilter = new StreamFilter();

            FromDatePicker.SelectedDate = new DateTime(2016, 4, 1);
            ToDatePicker.SelectedDate = new DateTime(2016, 8, 1);
        }
       
        private void OnGenerateCommand(object sender, RoutedEventArgs e)
        {
            if (CheckForNullValues(new List<KeyValuePair<object, string>>
            {
                new KeyValuePair<object, string> (FromGenDatePicker.SelectedDate, "From date"),
                new KeyValuePair<object, string> (FromGenTimePicker.Value, "From time"),
                new KeyValuePair<object, string> (StepTxt.Text, "Step size"),
                new KeyValuePair<object, string> (RangeTxt.Text, "Time range") }))
            {
                return;
            }
            
            if (SinFunctionRadio.IsChecked == true)
            {
                if (CheckForNullValue(PeriodsTxt.Text, "Period"))
                {
                    return;
                }

                var step = int.Parse(StepTxt.Text);
                var from = TapHomeTime.ToTapHomeTime(FromGenDatePicker.SelectedDate.Value.Add(FromGenTimePicker.Value.Value.TimeOfDay));
                var durationinSec = int.Parse(RangeTxt.Text)*60*60;
                var periods = int.Parse(PeriodsTxt.Text);

                _data = _dataGenerator.GenerateTemperatureDataSet(step, FuncType.Sin, from, durationinSec, periods, 0, 20);
                
            }

            else if (RandomFunctionRadio.IsChecked == true)
            {

                var step = int.Parse(StepTxt.Text);

                var fromDate = FromGenDatePicker.SelectedDate.Value.Date;
                var from = TapHomeTime.ToTapHomeTime(fromDate.Add(FromGenTimePicker.Value.Value.TimeOfDay));

                var durationinSec = int.Parse(RangeTxt.Text)*60*60;

                _data = _dataGenerator.GenerateTemperatureDataSet(step, FuncType.Random, from, durationinSec, 0, 0, 20);
                
            }
            else
            {
               ShowMessageBox("Function is not set.");
               return;
            }
            
            ShowSeries(_data, oxyplot);

        }

       

        private async void OnDownloadCommand(object sender, RoutedEventArgs e)
        {
            if (CheckForNullValues(new List<KeyValuePair<object, string>>
            {
                new KeyValuePair<object, string> (SensorCombo.Text, "Sensor "),
                new KeyValuePair<object, string> (FromTimePicker.Value, "From time"),
                new KeyValuePair<object, string> (FromDatePicker.SelectedDate, "From date"),
                new KeyValuePair<object, string> (ToTimePicker.Value, "To time"),
                new KeyValuePair<object, string> (ToDatePicker.SelectedDate, "To date"),
                new KeyValuePair<object, string> (GranularityCombo.Text, "Step size") }))
            {
                return;
            }
            

            var fromDate = FromDatePicker.SelectedDate.Value.Date;
            var fromTapHomeTime = TapHomeTime.ToTapHomeTime(fromDate.Add(FromTimePicker.Value.Value.TimeOfDay));

            var toDate = ToDatePicker.SelectedDate.Value.Date;
            var toTapHomeTime = TapHomeTime.ToTapHomeTime(toDate.Add(ToTimePicker.Value.Value.TimeOfDay));
            var deviceId = int.Parse(SensorCombo.Text);
            var granularity = Granularity.GetCodeFromName(GranularityCombo.Text);

            
            var downloadedData = await FetchDataFromStorage(deviceId, fromTapHomeTime, toTapHomeTime, granularity, "a");
            
            if (downloadedData == null || downloadedData.Count == 0)
            {
                ShowMessageBox("No data.");
                //((LineSeries) OriginalDataChart.Series[0]).ItemsSource = null;
                return;
            }

            _data = downloadedData.ToDictionary(kp => kp.TimeStamp, kp => (float)kp.Value);
            

           ShowSeries(_data, oxyplot);
        }

        private async Task<List<SenzorEntryModel>> FetchDataFromStorage(int deviceId, int from, int to, string granularity, string valueFunction)
        {
            using (var client = new HttpClient())
            {
                var url =
                    $"{apiUrl}/api/sensorentries/?locationId={"Test"}&deviceId={deviceId}&valueTypeId={1}&from={from}&to={to}&granularity={granularity}&valueFunction={valueFunction}";
                var response = client.GetAsync(url).Result;

                var json = await response.Content.ReadAsStringAsync();

                try
                {
                    return JsonConvert.DeserializeObject<List<SenzorEntryModel>>(json);
                }
                catch (JsonSerializationException)
                {
                    return null;
                }
            }
        }

        private void OnFilterCommand(object sender, RoutedEventArgs e)
        {
            if (_data == null)
            {
                ShowMessageBox("No data to filter.");
                return;
               
            } 

            var filterValue = int.Parse(FilterTxt.Text);

            FilterSettings filterSettings = new FilterSettings(FilterType.AnalogDataFilter, filterValue);

            _filteredData = _streamFilter.FilterAnalogData(_data, filterSettings);

            ShowSeries(_filteredData, oxyplot_filtered);
            
        }

        private async void OnUploadCommand(object sender, RoutedEventArgs e)
        {
            if (uploadOriginalRadio.IsChecked != null &&
                uploadFilteredRadio.IsChecked != null &&
                 (uploadFilteredRadio.IsChecked.Value || uploadOriginalRadio.IsChecked.Value))
            {
                IList<SenzorEntryModel> transformedData;

                if (uploadOriginalRadio.IsChecked.Value && _data != null && _data.Count != 0)
                {
                    transformedData = _data.Select(
                    kp => new SenzorEntryModel(new ChartValueDescription(), Guid.Empty, 1111, 1, Guid.NewGuid(),
                        Granularity.CoreValue, ValueFunction.Average, kp.Key, kp.Value)).ToList();
                    
                }
                else if (uploadFilteredRadio.IsChecked.Value && _filteredData != null && _filteredData.Count != 0)
                {
                    transformedData = _filteredData.Select(
                        kp => new SenzorEntryModel(new ChartValueDescription(), Guid.Empty, 1111, 1, Guid.NewGuid(),
                            Granularity.CoreValue, ValueFunction.Average, kp.Key, kp.Value)).ToList();
                }
                else
                {
                    ShowMessageBox("No data to upload");
                    return;
                }

                var response = await SendDataToStorage(transformedData);

                ShowMessageBox(response.Item2 != string.Empty ? response.Item2 : "Data successfully uploaded!");

            }
            else
            {
                ShowMessageBox("Choose which type of data to upload.");
            }
        }

        private async Task<Tuple<bool, string>> SendDataToStorage(IList<SenzorEntryModel> data)
        {
            using (var client = new HttpClient())
            {
                var response = client.PostAsync($"{apiUrl}/api/sensorentries",
                    new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")).Result;

                var success = response.IsSuccessStatusCode;

                var status = await response.Content.ReadAsStringAsync();

                return new Tuple<bool, string>(success, status);
            }
        }

        private void OnBrowseCommand(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                DefaultExt = ".tsv",
                Filter = "TSV files (*.tsv)|*.tsv"
            };


            var result = dialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                string filename = dialog.FileName;
                FileTxt.Text = filename;
            }
        }

        private void OnImportCommand(object sender, RoutedEventArgs e)
        {
            if (CheckForNullValue(FileTxt, "Import file "))
            {
                return;
            }

            var path = new Uri(FileTxt.Text).LocalPath;

            _data = _dataGenerator.ImportDataset(@path);

            if (_data == null || _data.Count == 0)
            {
                ShowMessageBox("Selected file has incorrect format.");
                return;
            }

            ShowSeries(_data, oxyplot);
        }


        private void ShowSeries(IDictionary<int, float> data, PlotView plot)
        {
           // WPF Toolkit charts
           // var readableData = data.Where(x => !double.IsNaN(x.Value)).ToDictionary(kp => TapHomeTime.FromTapHomeTime(kp.Key), kp => kp.Value);
           //((LineSeries)chart.Series[0]).ItemsSource = readableData;

            // OxyPlot charts
            
            var model = new LinePlotModel(data);

            plot.Model = model.Model;

            if (_data != null && _data.Count > 0)
            {
                SetNumberOfEntriesLabel();
            }
            if (_filteredData != null && _filteredData.Count > 0)
            {
                SetFilteredNumberOfEntriesLabel();
            }
        }

        private void SetNumberOfEntriesLabel()
        {
            OriginalDataLabel.Content = "Number of entries total: " + _data.Count;
        }

        private void SetFilteredNumberOfEntriesLabel()
        {
            FilteredDataLabel.Content = "Number of entries filtered: " + _filteredData.Count;
        }


        private bool CheckForNullValues(IList<KeyValuePair<object, string>> values)
        {
            StringBuilder message = new StringBuilder();

            foreach (var value in values.Where(value => value.Key == null || value.Key.Equals("")))
            {
                message.AppendLine(value.Value + " is not set.");
            }

            if (message.Length > 0)
            {
                ShowMessageBox(message.ToString());
                return true;
            }

            return false;
        }

        private bool CheckForNullValue(object value, string text)
        {
            if (value == null || value.Equals("")) { 
                ShowMessageBox(text + "is not set.");
                return true;
            }

            return false;
        }

        private void ShowMessageBox(string message)
        {
            MessageBox.Show(message, "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }



        // OBSOLETE
        private void OpenBrowser()
        {
            string curDir = Environment.CurrentDirectory;

            // load script form file for Highcharts
            StringBuilder script = new StringBuilder();

            foreach (string line in File.ReadLines($"{curDir}/highcharts/spline_form.html"))
            {
                script.AppendLine(line);
            }

            // insert data to specific place
            StringBuilder dataString = new StringBuilder();
            foreach (var d in _data)
            {
                var date = TapHomeTime.FromTapHomeTime(d.Key);
                dataString.AppendFormat("[Date.UTC({0}, {1}, {2}, {3}, {4}, {5}), {6}], ", date.Year, date.Month - 1, date.Day, date.Hour, date.Minute, date.Second, d.Value.ToString("F2",
                  CultureInfo.CreateSpecificCulture("en-US")));
            }

            script.Replace("__replacestring__", dataString.ToString());

            // write back to file
            using (var file = new StreamWriter(File.Create($"{curDir}/highcharts/index.html")))
            {
                file.Write(script.ToString());
            }

            // load page
            //Browser.Navigate(new Uri($"file:///{curDir}/highcharts/index.html"));
        }
        
    }
}

    


// C:\Users\janim\Documents\Projects\ChartingPrototype\Sandbox.TestApp\bin\Debug
//  using (var file = new FileStream($"{curDir}/highcharts/index.html", FileMode.Open))
    