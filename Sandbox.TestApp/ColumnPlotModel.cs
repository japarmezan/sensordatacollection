﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot.Axes;
using Sandbox.TestApp.Utils;

namespace Sandbox.TestApp
{
    using System;

    using OxyPlot;
    using OxyPlot.Series;

    public class ColumnPlotModel
    {
        public ColumnPlotModel(IDictionary<int, float> data)
        {
            Model = new PlotModel();
            var oxyplotData = data.Where(x => !double.IsNaN(x.Value)).Select(x => DateTimeAxis.CreateDataPoint(TapHomeTime.FromTapHomeTime(x.Key), Math.Round(x.Value, 2))).ToList();

            Model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, TickStyle = TickStyle.Outside });
            
            Model.Axes.Add(new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                StringFormat = "HH:mm",
                IntervalType = DateTimeIntervalType.Auto,
                MinorIntervalType = DateTimeIntervalType.Auto
            });


            var l = new ColumnSeries
            {
                FillColor = OxyColors.Blue,
                Selectable = true,
                SelectionMode = SelectionMode.Single
            };

            
            var series = new ColumnSeries();
            Model.Series.Add(series);

            series.ItemsSource = oxyplotData;

            Model.SelectionColor = OxyColors.Red;
            //l.TrackerFormatString = "{2}: {4}";
        }

        public PlotModel Model { get; private set; }
    }
}
