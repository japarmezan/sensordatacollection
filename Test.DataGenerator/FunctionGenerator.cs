﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    public class FunctionGenerator
    {
        public static IDictionary<int, float> GenerateSinFunction(int step, int min, int max, int from, int to, int period)
        {
            float f = 1/(float) period;
            float ampl = (float)(max - min) / 2;
            float delta = min + ampl;

            IDictionary<int, float> data = new Dictionary<int, float>();

            for (int i = from; i <= to; i = i + step)
            {
                data.Add(i, (float) (ampl * Math.Sin(2*f * Math.PI * (float)i + from) + delta));
            }

            return data;
        }
    }
}
