﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGenerator.Utils;

namespace DataGenerator
{
    public interface IDataGenerator
    {
        IDictionary<int, float> GenerateTemperatureDataSet(int stepInSeconds, FuncType functionType, int startTapHomeTime, int durationInSec, int periods, int minTemperature, int maxTemperature);

        IDictionary<int, float> GeneratePresenceSensorDataSet(int start, int end);

        IDictionary<int, float> ImportDataset(string path);
    }
}
