﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloud.Services.DataEntities.Utils;
using DataGenerator.Utils;
using RecycleBin.TextTables;

namespace DataGenerator
{
    public class DataGenerator : IDataGenerator
    {

        public IDictionary<int, float> GenerateTemperatureDataSet(int stepInSeconds, FuncType functionType, int startTapHomeTime, int durationInSec, int periods, int minTemperature, int maxTemperature)
        {
            IDictionary<int, float> data = new Dictionary<int, float>();
            var tapHomeTime = startTapHomeTime == 0 ? TapHomeTime.TapHomeNow() : startTapHomeTime;

            switch (functionType)
            {
                case FuncType.Sin:
                    var period = durationInSec/periods;

                    var rawData = FunctionGenerator.GenerateSinFunction(stepInSeconds, minTemperature, maxTemperature, 0, durationInSec, period);
                    foreach (var d in rawData)
                    {
                        data.Add(d.Key + tapHomeTime, d.Value);
                    }
                    break;

                case FuncType.Random:
                    Random rnd = new Random();
                    var range = maxTemperature - minTemperature;
                    double value = rnd.Next(minTemperature, maxTemperature);
                    var change = rnd.NextDouble() * Math.Sqrt(range);
                     
                    for (int i = 0; i < durationInSec; i = i + stepInSeconds)
                    {
                        do
                        {
                            change = (rnd.NextDouble() * 2 - 1) * Math.Sqrt(range);
                        } while (value + change > maxTemperature || value + change < minTemperature);

                        value = value + change;
                        data.Add(i + tapHomeTime, (float)Math.Round(value, 2));
                    }
                    break;
                    
                default: throw new InvalidEnumArgumentException("Function type not recognized");
            }

            return data;
        }

        public IDictionary<int, float> GeneratePresenceSensorDataSet(int start, int end)
        {
            IDictionary < int, float> data = new Dictionary<int, float>();
            var tapHomeTime = TapHomeTime.TapHomeNow();
            Random rnd = new Random(); 

            for (int i = start; i < end; i++)
            {
                data.Add(i + tapHomeTime, rnd.Next(10) < 2 ? 1 : 0);
            }

            return data;
        }

        public IDictionary<int, float> ImportDataset(string path)
        {
            var data = new Dictionary<int, float>();
            using(var csv = new CsvReader(path, new CsvReaderSettings {FieldDelimiter = "\u0009", QuotationCharacter = '"', RecordDelimiter = EndOfLine.LF}))
            {
                csv.HandleHeaderRow();
                csv.HandleHeaderRow();

                while (csv.MoveNext())
                {
                    var line = csv.Current;

                    try
                    {
                        var date = DateTime.ParseExact(line[0], "G", CultureInfo.InvariantCulture);
                        var timestamp = TapHomeTime.ToTapHomeTime(date.AddYears(1));

                        float value = (float) Convert.ToDouble(line[1], new CultureInfo("en-US"));

                        data.Add(timestamp, value);
                    }
                    catch (FormatException)
                    {
                        return null;
                    }
                }
            }

            return data;
        }
    }
}
