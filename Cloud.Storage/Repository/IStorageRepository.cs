﻿using System;
using System.Collections.Generic;
using Cloud.Storage.Entities;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cloud.Storage.Repository
{
    public interface IStorageRepository
    {
        void Insert(IList<SenzorEntryEntity> entities, string tableName);

        IEnumerable<SenzorEntryEntity> GetRange(string locationId, string deviceId, int valueTypeId, int from, int to, string granularity, string valueFunction);

        IEnumerable<object> GetRanges(string locationId, string deviceId, string valueTypeId, List<int> from,
            List<int> to, string granularity, string valueFunction);

        IEnumerable<object> GetAll(string tableName);

        bool DeleteTable(string tableName);

        string GetQueryPermissionSas(string tableName);

        bool RemoveQueryPermissionSas(string tableName);
    }
}
