﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Cloud.Storage.Entities;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cloud.Storage.Repository
{
    public class TableStorageRepository : IStorageRepository
    {
        
        public IEnumerable<SenzorEntryEntity> GetRange(string locationId, string deviceId, int valueTypeId, int from, int to, string granularity, string valueFunction)
        {

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the CloudTable object that represents the "people" table.
            CloudTable table = tableClient.GetTableReference(locationId);

            string partitionKey = deviceId + "_" + granularity;
            string rowKeyFrom = valueFunction + "_" + valueTypeId + "_" + from.ToString("D10");
            string rowKeyTo = valueFunction + "_" + valueTypeId + "_" + to.ToString("D10");

            var partitionKeyCondition = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);
            var rowKeyConditionFrom = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, rowKeyFrom);
            var rowKeyConditionTo = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, rowKeyTo);


            // Create the table query.
            TableQuery<SenzorEntryEntity> rangeQuery = new TableQuery<SenzorEntryEntity>().Where(
                TableQuery.CombineFilters(
                    partitionKeyCondition,
                    TableOperators.And,
                    TableQuery.CombineFilters(
                        rowKeyConditionFrom, 
                        TableOperators.And, 
                        rowKeyConditionTo))
                    );

            
            // Loop through the results, displaying information about the entity.
            return table.ExecuteQuery(rangeQuery);
        }

        public IEnumerable<object> GetRanges(string locationId, string deviceId, string valueTypeId, List<int> from, List<int> to, string granularity, string valueFunction)
        {

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the CloudTable object that represents the table.
            CloudTable table = tableClient.GetTableReference(locationId);

            // prepare key strings
            string partitionKey = deviceId + "_" + granularity;
            string[] rowKeyFrom = from.Select(x => valueFunction + "_" + valueTypeId + "_" + x.ToString("D10")).ToArray();
            string[] rowKeyTo = to.Select(x => valueFunction + "_" + valueTypeId + "_" + x.ToString("D10")).ToArray();

            // partition key condition
            var partitionKeyCondition = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                partitionKey);

            // combine row key conditions, i.e. (from1 & to1) | (from2 & to2) | ...
            var firstRowKeyConditionFrom = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, rowKeyFrom[0]);
            var firstRowKeyConditionTo = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, rowKeyTo[0]);
            
            var filter = TableQuery.CombineFilters(firstRowKeyConditionFrom, TableOperators.And, firstRowKeyConditionTo);

            for (int i = 1; i < rowKeyFrom.Length; i++)
            {
                var rowKeyConditionFrom = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, rowKeyFrom[i]);
                var rowKeyConditionTo = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, rowKeyTo[i]);

                filter = TableQuery.CombineFilters(filter, TableOperators.Or,
                    TableQuery.CombineFilters(rowKeyConditionFrom, TableOperators.And, rowKeyConditionTo));
            }
            
            
            // Create the table query.
            TableQuery<SenzorEntryEntity> rangeQuery = new TableQuery<SenzorEntryEntity>().Where(
                TableQuery.CombineFilters(
                    partitionKeyCondition,
                    TableOperators.And, 
                    filter));

            // execute query
            return table.ExecuteQuery(rangeQuery);
        }

        public IEnumerable<object> GetAll(string tableName)
        {
            // Retrieve the storage account from the connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            
            CloudTable table = tableClient.GetTableReference(tableName);
            
            TableQuery<SenzorEntryEntity> query = new TableQuery<SenzorEntryEntity>();
            
            return table.ExecuteQuery(query);
        }

        public void Insert(IList<SenzorEntryEntity> entities, string tableName)
        {

            var conn = ConfigurationManager.AppSettings["StorageConnectionString"];
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            
            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference(tableName);
            table.CreateIfNotExists();
            
            // Upload entities by partition keys
            var partitionKeyGroups = entities.GroupBy(x => x.PartitionKey);
             
            foreach (var @group in partitionKeyGroups)
            {
                TableBatchOperation batchOperation = new TableBatchOperation();

                foreach (var entity in @group)
                {
                    batchOperation.InsertOrMerge(entity);
                }
                table.ExecuteBatch(batchOperation);
                
            }
        }

        public bool DeleteTable(string tableName)
        {
            var conn = ConfigurationManager.AppSettings["StorageConnectionString"];
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference(tableName);

            return table.DeleteIfExists();
        }

        public string GetQueryPermissionSas(string tableName)
        {
            var conn = ConfigurationManager.AppSettings["StorageConnectionString"];
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference(tableName);
            table.CreateIfNotExists();

            TablePermissions tablePermissions = table.GetPermissions();

            // Clear the container's shared access policies to avoid naming conflicts.
            tablePermissions.SharedAccessPolicies.Clear();

            // The new shared access policy provides read/write access to the container for 24 hours.
            tablePermissions.SharedAccessPolicies.Add(tableName, new SharedAccessTablePolicy
            {
                // To ensure SAS is valid immediately, don’t set the start time.
                // This way, you can avoid failures caused by small clock differences.
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24),
                Permissions = SharedAccessTablePermissions.Query
            });

            // Set the new stored access policy on the container.
            table.SetPermissions(tablePermissions);

            // Get the shared access signature token to share with users.
            string sasToken;

            try
            {
                sasToken = table.GetSharedAccessSignature(new SharedAccessTablePolicy(), tableName);
            }
            catch (InvalidOperationException)
            {
                return null;
            }

            return sasToken;
        }

        public bool RemoveQueryPermissionSas(string tableName)
        {
            var conn = ConfigurationManager.AppSettings["StorageConnectionString"];
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn);

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference(tableName);
            table.CreateIfNotExists();

            TablePermissions tablePermissions = table.GetPermissions();

            // Clear the container's shared access policies to avoid naming conflicts.
            return tablePermissions.SharedAccessPolicies.Remove(tableName);
        }
    }
}
