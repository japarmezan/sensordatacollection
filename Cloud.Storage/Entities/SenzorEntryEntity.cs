﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cloud.Storage.Entities
{
    public class SenzorEntryEntity : TableEntity
    {
        public SenzorEntryEntity(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
        }

        public SenzorEntryEntity()
        {
        }

        public double Value { get; set; }
        public Guid SessionId { get; set; }
    }
}
