//using System.Collections.Generic;
//using Android.Graphics;
//using Com.Syncfusion.Charts;
//using Java.Util;
//using Mobile.Chart.Droid.OxyplotChart;
//using Mobile.Chart.Droid.OxyplotChart.SeriesContainers;
//using Mobile.Chart.Droid.SeriesDefinitions;

//namespace Mobile.Chart.Droid.SyncfusionChart
//{
//    internal class SensorSeries
//    {
//        private readonly SfChart chart;

//        public SensorSeries(SensorInfo sensorInfo, SfChart chart)
//        {
//            SensorInfo = sensorInfo;
//            this.chart = chart;
//            SeriesParts = new Dictionary<LoadedTimespan, ChartSeries>();
//        }

//        public SensorInfo SensorInfo { get; }

//        internal IDictionary<LoadedTimespan, ChartSeries> SeriesParts { get; }

//        public void AddSeriesPart(LoadedTimespan loadedTimespan, ArrayList datapoints)
//        {
//            FastLineSeries series = new FastLineSeries
//            {
//                DataSource = datapoints,
//                AnimationEnabled = true,
//                DataPointSelectionEnabled = true,
//                SelectedDataPointColor = Color.Red
//            };

//            chart.Series.Add(series);

//            SeriesParts.Add(loadedTimespan, series);
//        }

//        public void RemoveSeriesPartByTimespan(LoadedTimespan timespan)
//        {
//            var seriesPart = SeriesParts[timespan];
//            chart.Series.Remove(seriesPart);
//            SeriesParts.Remove(timespan);
//        }

//        public void RemoveAllSeriesParts()
//        {
//            foreach (var seriesPart in SeriesParts)
//            {
//                chart.Series.Remove(seriesPart.Value);
//                SeriesParts.Clear();
//            }
//        }
        

//        public void ChangeDataPoints(IDictionary<LoadedTimespan, ArrayList> newDataPoints)
//        {
//            foreach (var dataPoints in newDataPoints )
//            {
//                SeriesParts[dataPoints.Key].DataSource = dataPoints.Value;
//            }
//        }

//        public override bool Equals(object obj)
//        {
//            SensorSeries s = obj as SensorSeries;

//            if (s == null)
//            {
//                return false;
//            }

//            return SensorInfo.Equals(s.SensorInfo);
//        }

//        public override int GetHashCode()
//        {
//            return base.GetHashCode();
//        }
//    }
//}