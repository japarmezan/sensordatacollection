//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;
//using Android.App;
//using Android.Content;
//using Android.Views;
//using Android.Widget;
//using Cloud.Services.DataEntities.SensorEntry;
//using Cloud.Services.DataEntities.Utils;
//using Com.Syncfusion.Charts;
//using Com.Syncfusion.Charts.Enums;
//using Java.Util;
//using Mobile.Chart.Droid.OxyplotChart;
//using Mobile.Chart.Droid.OxyplotChart.SeriesContainers;
//using Mobile.Chart.Droid.SeriesDefinitions;
//using Mobile.Chart.Droid.Services;
//using Mobile.Chart.Droid.Utils;

//namespace Mobile.Chart.Droid.SyncfusionChart
//{
//    public class SyncfusionChartRenderer : ChartRenderer, IChartRenderer
//    {
//        private readonly IDataService dataService;

//        private readonly SfChart Chart;

//        private readonly IList<SensorSeries> ActiveSeries;
//        private readonly IList<LoadedTimespan> LoadedTimespans;

//        private bool loaded = false;

//        private float CurrentZoom = 0.1f;

//        private float PreviousZoom = 0.1f;

//        private float CurrentZoomPosition;
        
//        public string CurrentGranularity { get; set; } = Granularity.CoreValue;

//        public string CurrentLabelFormat { get; set; } = "dd/MM/yy HH:mm:ss";

//        public DateTime LoadedMaximum { get; set; } = new DateTime(2016, 7, 21, 12, 0, 0);

//        public DateTime LoadedMinimum { get; set; } = new DateTime(2016, 7, 21, 6, 0, 0);


//        public bool EnableDynamicDataChange { get; set; } = true;


//        public SyncfusionChartRenderer(Context context)
//        {
//            ChartContext = context;
//            ChartActivity = (Activity) context;

//            Chart = new SfChart(ChartContext);
//            dataService = new KeySignatureDataService();
//            ActiveSeries = new List<SensorSeries>();
//            LoadedTimespans = new List<LoadedTimespan> {new LoadedTimespan(LoadedMinimum, LoadedMaximum)};
            
//            SetUp();
//        }

//        private void SetUp()
//        {
//            DateTimeAxis pAxis = new DateTimeAxis
//            {
//                ZoomFactor = CurrentZoom,
//                RangePadding = DateTimeRangePadding.Auto,
//                ZoomPosition = 0.5f
//            };

//            pAxis.LabelStyle.LabelFormat = CurrentLabelFormat;
//            pAxis.LabelsIntersectAction = AxisLabelsIntersectAction.Hide;

//            Chart.PrimaryAxis = pAxis;

//            NumericalAxis qAxis = new NumericalAxis();
//            Chart.SecondaryAxis = qAxis;

//            ChartZoomPanBehavior zp = new ChartZoomPanBehavior
//            {
//                ScrollingEnabled = true,
//                DoubleTapEnabled = false,
//                ZoomMode = ZoomMode.X
//            };

//            Chart.Behaviors.Add(zp);

//            CustomTrackBall showTrackBall = new CustomTrackBall
//            {
//                LabelDisplayMode = TrackballLabelDisplayMode.FloatAllPoints
//            };

//            Chart.Behaviors.Add(showTrackBall);

//            Chart.ZoomStart += ZoomEventHandler;

//            Chart.Scroll += (sender, args) =>
//            {
//                if (!loaded && Chart.PrimaryAxis.ZoomPosition < 0.1)
//                {
//                    loaded = true;

//                    AddTimespan();
                   
//                    Chart.PrimaryAxis.ZoomPosition = 0.5f;


//                }
//                Console.WriteLine("Position: " + Chart.PrimaryAxis.ZoomPosition);
//            };

//        }

//        private void AddTimespan()
//        {
//            var timespan = new LoadedTimespan(new DateTime(2016, 7, 21, 0, 0, 0), new DateTime(2016, 7, 21, 6, 0, 0));
//            LoadedTimespans.Add(timespan);

//            ThreadUtils.DoWithLoadingDialog(ChartActivity, async () =>
//            {
//                foreach (var series in ActiveSeries)
//                {
//                    var datapoints = await FetchDataForTimespan(series.SensorInfo, new DateTime(2016, 7, 21, 0, 0, 0),
//                        new DateTime(2016, 7, 21, 6, 0, 0));

//                    ThreadUtils.ManageChart(ChartActivity, () => series.AddSeriesPart(timespan, datapoints));
//                }
//            });
            
//        }
        

//        private void RemoveLastTimespan()
//        {
//            var removeTimespan = LoadedTimespans.Last();

//            foreach (var series in ActiveSeries)
//            {
//                series.RemoveSeriesPartByTimespan(removeTimespan);
//            }

//            LoadedTimespans.Remove(removeTimespan);
//        }
        

//        private void ZoomEventHandler(object sender, SfChart.ZoomStartEventArgs a)
//        {
//            if (!EnableDynamicDataChange) return;

//            var chart = sender as SfChart;

//            if (chart == null) return;

//            CurrentZoom = Chart.PrimaryAxis.ZoomFactor;

//            Console.WriteLine("Previous: " + PreviousZoom);
//            Console.WriteLine("Current: " + CurrentZoom);

//            if (true)
//            {
//                if (Math.Abs(CurrentZoom - PreviousZoom) > 0.01)
//                {
//                    CurrentGranularity = CurrentZoom > PreviousZoom ? Granularity.GetHigherLevel(CurrentGranularity) : Granularity.GetLowerLevel(CurrentGranularity);

//                    CurrentZoomPosition = a.P1.CurrentZoomPosition;

//                    Console.WriteLine("Granularity: " + CurrentGranularity);
                    
//                    foreach (var series in ActiveSeries)
//                    {
//                        UpdateSeries(series);
//                    }

//                    PreviousZoom = CurrentZoom;
//                }
//            }
            
//        }

//        public class CustomTrackBall : ChartTrackballBehavior
//        {
//            protected override void OnDoubleTap(float x, float y)
//            {
//                Show(x, y);
//            }
//        }

//        public void Render(ViewGroup view)
//        {
//            view.AddView(Chart);
//        }

//        public void AddSeries(SensorInfo sensorInfo, IList<string> allowedGranularities)
//        {
//            if (sensorInfo == null)
//            {
//                throw new ArgumentNullException(nameof(sensorInfo));
//            }

//            if (ActiveSeries.Any(x => x.SensorInfo.Equals(sensorInfo)))
//            {
//                throw new ArgumentException(nameof(sensorInfo) + " already exists in chart.");
//            }

//            ThreadUtils.DoWithLoadingDialog(ChartActivity, async () =>
//            {
//                var dataPoint = await FetchDataChunksForWholeTimespan(sensorInfo);

//                var sensorSeries = new SensorSeries(sensorInfo, Chart);

//                foreach (var chunk in dataPoint)
//                {
//                    ThreadUtils.ManageChart(ChartActivity, () => sensorSeries.AddSeriesPart(chunk.Key, chunk.Value));
//                }

//                ThreadUtils.ManageChart(ChartActivity, () => ActiveSeries.Add(sensorSeries));
//            });

            
//        }

//        public void RemoveSeries(SensorInfo sensorInfo)
//        {
//            if (sensorInfo == null)
//            {
//                throw new ArgumentNullException(nameof(sensorInfo));
//            }

//            var series = ActiveSeries.First(x => x.SensorInfo.Equals(sensorInfo));
//            series.RemoveAllSeriesParts();

//            ActiveSeries.Remove(series);
//        }

//        private async void UpdateSeries(SensorSeries series)
//        {
//            var newData = await FetchDataChunksForWholeTimespan(series.SensorInfo);

//            if (newData == null || newData.Any(x => x.Value == null) || newData.All(x => x.Value.Size() == 0))
//            {
//                return;
//            }
            
//            series.ChangeDataPoints(newData);

//            Chart.PrimaryAxis.ZoomFactor = CurrentZoom;
//        }

//        private async Task<IDictionary<LoadedTimespan, ArrayList>> FetchDataChunksForWholeTimespan(SensorInfo sensorInfo)
//        {
//            if (sensorInfo == null)
//            {
//                throw new ArgumentNullException(nameof(sensorInfo));
//            }

//            var from = TapHomeTime.ToTapHomeTime(LoadedMinimum);
//            var to = TapHomeTime.ToTapHomeTime(LoadedMaximum);

//            var fetchedData = await dataService.GetRange(sensorInfo.LocationId, sensorInfo.DeviceId, sensorInfo.ValueTypeId, from, to, CurrentGranularity, ValueFunction.Average);

//            IDictionary<LoadedTimespan,ArrayList> dataPointChunks = new Dictionary<LoadedTimespan, ArrayList>();

//            foreach (var loadedTimespan in LoadedTimespans)
//            {
//                var dataChunk =
//                    fetchedData.TakeWhile(x => TapHomeTime.FromTapHomeTime(x.TimeStamp) <= loadedTimespan.End).ToArray();

//                var dataPointsChunk = new ArrayList();

//                foreach (var senzorEntryModel in dataChunk)
//                {
//                    var date = TapHomeTime.FromTapHomeTime(senzorEntryModel.TimeStamp);
//                    dataPointsChunk.Add(new ChartDataPoint(new Date(date.Year, date.Month - 1, date.Day, date.Hour, date.Minute, date.Second), senzorEntryModel.Value));
//                    fetchedData.Remove(senzorEntryModel);
//                }

//                dataPointChunks.Add(loadedTimespan, dataPointsChunk);
//            }

//            return dataPointChunks;
//        }

//        private async Task<ArrayList> FetchDataForTimespan(SensorInfo sensorInfo, DateTime from, DateTime to)
//        {
//            if (sensorInfo == null)
//            {
//                throw new ArgumentNullException(nameof(sensorInfo));
//            }

//            ArrayList data = new ArrayList();
//            var fetchedData = await dataService.GetRange(sensorInfo.LocationId, sensorInfo.DeviceId, sensorInfo.ValueTypeId, TapHomeTime.ToTapHomeTime(from), TapHomeTime.ToTapHomeTime(to), CurrentGranularity, ValueFunction.Average);
            
//            foreach (var model in fetchedData)
//            {
//                var date = TapHomeTime.FromTapHomeTime(model.TimeStamp);
//                data.Add(
//                    new ChartDataPoint(
//                        new Date(date.Year, date.Month - 1, date.Day, date.Hour, date.Minute, date.Second), model.Value));
//            }
//            return data;
//        }
//    }
    
    
//}