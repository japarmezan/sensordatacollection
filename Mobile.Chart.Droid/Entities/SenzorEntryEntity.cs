﻿using System;

namespace Mobile.Chart.Droid.Entities
{
    public class SenzorEntryEntity
    {
        
        public SenzorEntryEntity()
        {
        }

        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public double Value { get; set; }
        public Guid SessionId { get; set; }
    }
}
