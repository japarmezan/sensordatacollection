using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Cloud.Services.DataEntities.SensorEntry;
using Mobile.Chart.Droid.Entities;
using Newtonsoft.Json;

namespace Mobile.Chart.Droid.Services
{
    class KeySignatureDataService : IDataService
    {
        private static string AccountName { get; } = "senzorlogs";
        private static string UrlEndpoint { get; } = $"http://{AccountName}.table.core.windows.net/";
        private static string Key { get; } = "eBnrTf8Eur7AFPBN3BQCaMKXYfUemJ0QSj4Fjhsz3TMrvjC8/DpnPhxmagmNh3ky1srF4NV+jtyXpOAslBKZeA==";


        public async Task<IList<SenzorEntryModel>> GetRange(string locationId, string deviceId, string valueTypeId, int from, int to, string granularity, string valueFunction)
        {
            //working query
            //var query = "Test?$filter=(PartitionKey%20eq%20'1111_b')%20and%20((RowKey%20ge%20'1_0007862400')%20and%20(RowKey%20le%20'1_0018403200'))";
            
            var query = ConstructQuery(locationId, deviceId, valueTypeId, from, to, granularity, valueFunction);
            List<SenzorEntryEntity> entities = new List<SenzorEntryEntity>();

            try
            {
                using (var request = PrepareHttpClient(locationId)) { 

                    Stopwatch timer = new Stopwatch();
                    timer.Start();


                    var response = await request.GetAsync($"{UrlEndpoint}{query}");
                
                    timer.Stop();
                    System.Diagnostics.Debug.WriteLine("Get TableStorage: " + timer.ElapsedMilliseconds + " ms");


                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();

                        var definition = new {Value = new List<SenzorEntryEntity>()};
                        var json = JsonConvert.DeserializeAnonymousType(content, definition);
                        entities = json.Value;
                    }
                }
            }
            catch (Exception oEx)
            {
                Debug.WriteLine((oEx.InnerException.Message));
            }

            return TableEntityMapper.MapTableEntitiesToModels(entities);
        }

        private string ConstructQuery(string locationId, string deviceId, string valueTypeId, int from, int to, string granularity, string valueFunction)
        {
            string partitionKey = deviceId + "_" + granularity;
            string rowKeyFrom = valueFunction + "_" + valueTypeId + "_" + from.ToString("D10");
            string rowKeyTo = valueFunction + "_" + valueTypeId + "_" + to.ToString("D10");

            string query = $"{locationId}?$filter=(PartitionKey%20eq%20'{partitionKey}')%20and%20((RowKey%20ge%20'{rowKeyFrom}')%20and%20(RowKey%20le%20'{rowKeyTo}'))";

            return query;
        }

        private HttpClient PrepareHttpClient(string tableName)
        {
            var utcDate = DateTime.UtcNow.ToString("R");

            var authorizationStringToSign = utcDate + $"\n/{AccountName}/{tableName}";

            var signature = SignString(authorizationStringToSign);
            string authorizationString = $"SharedKeyLite {AccountName}:{signature}";

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", authorizationString);
            client.DefaultRequestHeaders.Add("x-ms-date", utcDate);
            client.DefaultRequestHeaders.Add("x-ms-version", "2015-12-11");
            client.DefaultRequestHeaders.Add("Accept", "application/json;odata=nometadata"); 

            return client;
        }

        private string SignString(string stringToSign)
        {
            var convertedKey = Convert.FromBase64String(KeySignatureDataService.Key);

            HMACSHA256 hmac = new HMACSHA256(convertedKey);

            var signature = hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign));

            return Convert.ToBase64String(signature);
        }


    }
}