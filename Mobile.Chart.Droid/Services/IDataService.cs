using System.Collections.Generic;
using System.Threading.Tasks;
using Cloud.Services.DataEntities.SensorEntry;

namespace Mobile.Chart.Droid.Services
{
    interface IDataService
    {
        Task<IList<SenzorEntryModel>> GetRange(string locationId, string deviceId, string valueTypeId, int from, int to,
            string granularity, string valueFunction);
    }
}