using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mobile.Chart.Droid.Services
{
    public class SasAccessToken
    {
        public DateTime ValidUntil { get; set; }

        public string Signature { get; set; }

        public SasAccessToken(DateTime validUntil, string signature, bool parse)
        {
            ValidUntil = validUntil;

            // remove ? and " signs, cause they are redundant
            if (parse)
            {
                var parsed = signature.Split('?');

                var parsedSignature = parsed.Length == 2 ? parsed[1] : parsed[0];

                if (parsedSignature[parsedSignature.Length - 1] == '\"')
                {
                    parsedSignature = parsedSignature.Remove(parsedSignature.Length - 1);
                }
                Signature = parsedSignature;
            }
            else
            {
                Signature = signature;
            }

        }
    }
}