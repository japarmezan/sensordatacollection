using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cloud.Services.DataEntities.SensorEntry;
using Mobile.Chart.Droid.Entities;
using Newtonsoft.Json;

namespace Mobile.Chart.Droid.Services
{
    public class AccessTokenDataService: IDataService
    {
        private static string AccountName { get; } = "senzorlogs";
        private static string UrlEndpoint { get; } = $"http://{AccountName}.table.core.windows.net/";
        
        private static string AccessTokenAccountName { get; } = "taphomecharttest";
        private static string AccessTokenApiPath { get; } = "api/signature/";

        private static string AccessTokenUrl { get; } = $"http://{AccessTokenAccountName}.azurewebsites.net/{AccessTokenApiPath}";

        public SasAccessToken AccessToken;
        
        public async Task<IList<SenzorEntryModel>> GetRange(string locationId, string deviceId, string valueTypeId, int from, int to, string granularity, string valueFunction)
        {
            // try getting values from Core

            // var coreStatistics = CommunicationManager.GetStatistics();
            //if (coreStatistics != null)
            //{
            //    // process statistics
            //}

            

            //working query
            //var query = "Test?$filter=(PartitionKey%20eq%20'1111_b')%20and%20((RowKey%20ge%20'1_0007862400')%20and%20(RowKey%20le%20'1_0018403200'))";

            Stopwatch timer = new Stopwatch();
            timer.Start();

            List<SenzorEntryEntity> entities = new List<SenzorEntryEntity>();
            
            using (var request = PrepareHttpClient())
            {
                var query = await ConstructQuery(locationId, deviceId, valueTypeId, from, to, granularity, valueFunction, false);

                var response = await request.GetAsync($"{UrlEndpoint}{query}");

                timer.Stop();
                System.Diagnostics.Debug.WriteLine("Get TableStorage: " + timer.ElapsedMilliseconds + " ms");
                    
                if (response.IsSuccessStatusCode)
                {
                    entities = await ParseResponse(response);
                }

                // 403 Forbidden, try renewing access token
                if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    query = await ConstructQuery(locationId, deviceId, valueTypeId, from, to, granularity, valueFunction, true);
                    response = await request.GetAsync($"{UrlEndpoint}{query}");

                    if (response.IsSuccessStatusCode)
                    {
                        entities = await ParseResponse(response);
                    }
                    else
                    {
                        throw new HttpRequestException("Data could not be downloaded");
                    }
                }
            }

            return TableEntityMapper.MapTableEntitiesToModels(entities);
        }

        private async Task<List<SenzorEntryEntity>> ParseResponse(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();

            var definition = new { Value = new List<SenzorEntryEntity>() };
            var json = JsonConvert.DeserializeAnonymousType(content, definition);
            return json.Value;
        }

        private async Task<string> ConstructQuery(string locationId, string deviceId, string valueTypeId, int from, int to, string granularity, string valueFunction, bool renewAccessToken)
        {
            string partitionKey = deviceId + "_" + granularity;
            string rowKeyFrom = valueFunction + "_" + valueTypeId + "_" + from.ToString("D10");
            string rowKeyTo = valueFunction + "_" + valueTypeId + "_" + to.ToString("D10");

            if (renewAccessToken || AccessToken == null || AccessToken.ValidUntil > DateTime.UtcNow)
            {
                AccessToken = await RenewAccessToken(locationId);
            }
            
            string query = $"{locationId}?$filter=(PartitionKey%20eq%20'{partitionKey}')%20and%20((RowKey%20ge%20'{rowKeyFrom}')%20and%20(RowKey%20le%20'{rowKeyTo}'))&{AccessToken.Signature}";

            return query;
        }

        public async Task<SasAccessToken> RenewAccessToken(string locationId)
        {
            HttpClient client = new HttpClient();

            var response = await client.GetAsync($"{AccessTokenUrl}?locationId={locationId}");
            string tokenString;

            if (response.IsSuccessStatusCode)
            { 
                tokenString = await response.Content.ReadAsStringAsync();
                return new SasAccessToken(DateTime.UtcNow.AddHours(24), tokenString, true);
            }

            var numberOfRetries = 2;

            while (!response.IsSuccessStatusCode && numberOfRetries > 0)
            {
                Thread.Sleep(3000);
                response = await client.GetAsync(AccessTokenUrl);
                numberOfRetries--;
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new TimeoutException("Cannot connect to " + AccessTokenUrl);
            }
            
            tokenString = await response.Content.ReadAsStringAsync();
            return new SasAccessToken(DateTime.UtcNow.AddHours(24), tokenString, true);
        }

        private HttpClient PrepareHttpClient()
        {
            var utcDate = DateTime.UtcNow.ToString("R");
            
            HttpClient client = new HttpClient();
            
            client.DefaultRequestHeaders.Add("x-ms-date", utcDate);
            client.DefaultRequestHeaders.Add("x-ms-version", "2015-12-11");
            client.DefaultRequestHeaders.Add("Accept", "application/json;odata=nometadata");

            return client;
        }
    }
}