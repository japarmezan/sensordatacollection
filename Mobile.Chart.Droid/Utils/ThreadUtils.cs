using System;
using System.Threading;
using Android.App;

namespace Mobile.Chart.Droid.Utils
{
    public static class ThreadUtils
    {
        //public static void DoWithLoadingDialog(Activity activity, Action action)
        //{
        //    var progressDialog = ProgressDialog.Show(activity, "", "Loading...", true);

        //    new Thread(new ThreadStart(delegate
        //    {
        //        action();

        //        //Activity.RunOnUiThread(() => Toast.MakeText(Context, "Data downloaded", ToastLength.Short).Show());
        //        activity.RunOnUiThread(() => progressDialog.Hide());

        //    })).Start();
        //}

        public static void DoWithLoadingDialog(Activity activity, Action action)
        {
            var progressDialog = ProgressDialog.Show(activity, "", "Loading...", true);
            
            action();

            progressDialog.Hide();
        }

        public static void ManageChart(Activity activity, Action action)
        {
            activity.RunOnUiThread(action);
        }
    }
}