using System;
using System.Runtime.Serialization;

namespace Mobile.Chart.Droid.Utils
{
    internal class ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException : Exception
    {
        public ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException()
        {
        }
        

        public ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException(string message) : base(message)
        {
        }

        public ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}