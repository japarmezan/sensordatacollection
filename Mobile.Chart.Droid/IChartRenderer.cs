﻿using System.Collections.Generic;
using Android.Views;
using Mobile.Chart.Droid.SeriesDefinitions;

namespace Mobile.Chart.Droid
{
    public interface IChartRenderer
    {
        void Render(ViewGroup view);

        void AddSeries(SensorInfo sensorInfo, IList<string> allowedGranularities);

        void RemoveSeries(SensorInfo sensorInfo);
    }
}
