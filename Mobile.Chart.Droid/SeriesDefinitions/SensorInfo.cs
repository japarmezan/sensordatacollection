﻿namespace Mobile.Chart.Droid.SeriesDefinitions
{
    public class SensorInfo
    {
        public SensorInfo(string locationId, string deviceId, string valueTypeId, string valueFunction)
        {
            LocationId = locationId;
            DeviceId = deviceId;
            ValueTypeId = valueTypeId;
            ValueFunction = valueFunction;
        }

        public string LocationId { get; }

        public string DeviceId { get; }

        public string ValueTypeId { get; }

        public string ValueFunction { get; }


        public override bool Equals(object obj)
        {
            SensorInfo s = obj as SensorInfo;

            if (s == null)
            {
                return false;
            }

            return LocationId.Equals(s.LocationId) && DeviceId.Equals(s.DeviceId) &&
                   ValueFunction.Equals(s.ValueFunction) && ValueTypeId.Equals(s.ValueTypeId);
        }

        protected bool Equals(SensorInfo other)
        {
            if (other == null)
            {
                return false;
            }

            return string.Equals(LocationId, other.LocationId) && string.Equals(DeviceId, other.DeviceId) && 
                string.Equals(ValueTypeId, other.ValueTypeId) && string.Equals(ValueFunction, other.ValueFunction);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (LocationId != null ? LocationId.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (DeviceId != null ? DeviceId.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ValueTypeId != null ? ValueTypeId.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ValueFunction != null ? ValueFunction.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
