using System;
using Cloud.Services.DataEntities.SensorEntry;

namespace Mobile.Chart.Droid.SeriesDefinitions
{
    public class ZoomThresholdDefinition
    {
        public TimeSpan CoreValueZoomThreshold { get; set; } = new TimeSpan(12, 0, 0);
        public TimeSpan HourBatchZoomThreshold { get; set; } = new TimeSpan(3, 0, 0, 0);
        public TimeSpan DayZoomThreshold { get; set; } = new TimeSpan(35, 0, 0, 0);
        
        public TimeSpan GetThreshold(string granularity)
        {
            switch (granularity)
            {
                case Granularity.CoreValue:
                    return CoreValueZoomThreshold;
                case Granularity.CoreHourBatch:
                    return HourBatchZoomThreshold;
                case Granularity.DayRegular:
                    return DayZoomThreshold;
                default:
                    return new TimeSpan(7, 0, 0, 0);

            }
        }
    }
}