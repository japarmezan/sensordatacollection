using System;
using Cloud.Services.DataEntities.SensorEntry;

namespace Mobile.Chart.Droid.SeriesDefinitions
{
    public class LoadTimespansDefinition
    {
        public TimeSpan CoreValueLoadTimeSpan { get; set; } = new TimeSpan(1, 0, 0, 0);

        public TimeSpan HourBatchLoadTimeSpan { get; set; } = new TimeSpan(7, 0, 0, 0);

        public TimeSpan DayRegularLoadTimeSpan { get; set; } = new TimeSpan(50, 0, 0, 0);

        public TimeSpan GetTimeSpan(string granularity)
        {
            switch (granularity)
            {
                case Granularity.CoreValue:
                    return CoreValueLoadTimeSpan;
                case Granularity.CoreHourBatch:
                    return HourBatchLoadTimeSpan;
                case Granularity.DayRegular:
                    return DayRegularLoadTimeSpan;
                default:
                    return new TimeSpan(7, 0, 0, 0);

            }
        }
    }
}