using System.Collections.Generic;
using OxyPlot;

namespace Mobile.Chart.Droid.SeriesDefinitions
{
    public class UserSeriesDefinition
    {
        public SensorInfo SensorInfo { get; set; }

        public IList<string> AllowedGranularities  { get; set; }
        
        public OxyColor Color { get; set; }

        public override bool Equals(object obj)
        {
            UserSeriesDefinition u = obj as UserSeriesDefinition;

            if (u == null)
            {
                return false;
            }

            return SensorInfo.Equals(u.SensorInfo);
        }

        public override int GetHashCode()
        {
            var hashCode = (SensorInfo.GetHashCode());
            return hashCode;
        }
    }
}