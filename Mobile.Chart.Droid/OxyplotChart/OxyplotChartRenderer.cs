﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Mobile.Chart.Droid.OxyplotChart.SeriesContainers;
using Mobile.Chart.Droid.SeriesDefinitions;
using MoreLinq;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;
using BarSeries = OxyPlot.Series.BarSeries;
using ColumnSeries = OxyPlot.Series.ColumnSeries;
using DateTimeAxis = OxyPlot.Axes.DateTimeAxis;
using LineSeries = OxyPlot.Series.LineSeries;

namespace Mobile.Chart.Droid.OxyplotChart
{
    public class OxyplotChartRenderer : ChartRenderer, IChartRenderer
    {
        private readonly PlotView PlotView;
        private PlotModel Chart;

        private bool rendered;

        private readonly OxyplotSeriesManager seriesManager;

        private readonly bool isSimpleChartAppearance;

        public LinearAxis VerticalAxis { get; set; }
        public DateTimeAxis HorizontalAxis { get; set; }
        
        private LineAnnotation tracker;
        internal LineAnnotation NowLineAnnotation;

        public DateTime AbsoluteMaximum
        {
            get { return seriesManager.AbsoluteMaximum; }
            set { seriesManager.AbsoluteMaximum = value; }
        }

        public DateTime AbsoluteMinimum
        {
            get { return seriesManager.AbsoluteMinimum; }
            set { seriesManager.AbsoluteMinimum = value; }
        }

        public LoadTimespansDefinition LoadTimespansDefinition
        {
            get { return seriesManager.LoadTimespansDefinition; }
            set { seriesManager.LoadTimespansDefinition = value; }
        }

        public OxyplotChartRenderer(Context context, bool isSimpleChartAppearance = false)
        {
            ChartContext = context;
            this.isSimpleChartAppearance = isSimpleChartAppearance;

            ChartActivity = (Activity) context;
            PlotView = new PlotView(context);

            SetUpChart();

            seriesManager = new OxyplotSeriesManager(this, Chart, ChartActivity);

            SetUpAnnotations();

            PlotView.Model = Chart;
        }

        private void SetUpChart()
        {
            Chart = ChartComponentsFactory.CreatePlotModel();

            if (isSimpleChartAppearance)
            {
                Chart.Axes.Add(ChartComponentsFactory.CreateLinearAxisSimpleAppearance());
                Chart.Axes.Add(ChartComponentsFactory.CreateDateTimeAxisSimpleAppearance());
            }
            else
            {
                Chart.Axes.Add(ChartComponentsFactory.CreateLinearAxis());
                Chart.Axes.Add(ChartComponentsFactory.CreateDateTimeAxis());
            }
            
            VerticalAxis = (LinearAxis) Chart.Axes[0];
            
            HorizontalAxis = (DateTimeAxis)Chart.Axes[1];

            HorizontalAxis.AxisChanged += HandleAxisChange;
        }

        private void SetUpAnnotations()
        {
            if (!isSimpleChartAppearance) { 
                NowLineAnnotation = ChartComponentsFactory.CreateAbsoluteMaximumLine(AbsoluteMaximum);

                Chart.Annotations.Add(NowLineAnnotation);

                tracker = ChartComponentsFactory.CreateTrackerLine();

                Chart.Annotations.Add(tracker);

                Chart.Updated += (sender, args) =>
                {
                    UpdateTrackerPosition();
                };
            }
        }

        private void HandleAxisChange(object sender, AxisChangedEventArgs e)
        {
            UpdateTrackerPosition();

            seriesManager.HandleAxisChange();
        }

        private double GetTrackerPosition()
        {
            return HorizontalAxis.ActualMaximum - GetTrackerOffset();
        }

        internal double GetTrackerOffset()
        {
            return (HorizontalAxis.ActualMaximum - HorizontalAxis.ActualMinimum)/3;
        }

        private string GetTrackerNearestValue()
        {
            if (tracker == null)
            {
                return "";
            }

            var xPositionOnAxis = tracker.X;
            var xPositionDate = DateTimeAxis.ToDateTime(xPositionOnAxis);

            return seriesManager.GetNearestDataPointValue(xPositionDate);
        }


        internal void UpdateTrackerPosition()
        {
            if (tracker == null || seriesManager.SeriesContainer.Count == 0 || Chart.Series.Count == 0) 
            {
                return;
            }

            tracker.X = GetTrackerPosition();
            tracker.Text = GetTrackerNearestValue();
        }

        public void Render(ViewGroup view)
        {
            if (rendered) return;

            seriesManager.FirstRender();

            view.AddView(PlotView);

            rendered = true;
        }

        public void AddSeries(SensorInfo sensorInfo, IList<string> allowedGranularities)
        {
            if (sensorInfo == null)
            {
                throw new ArgumentNullException(nameof(sensorInfo));
            }

            if (allowedGranularities == null || allowedGranularities.Count == 0)
            {
                throw new ArgumentNullException(nameof(allowedGranularities) + " is empty.");
            }
            
            seriesManager.AddUserSeriesDefinition(new UserSeriesDefinition { AllowedGranularities = allowedGranularities,  SensorInfo = sensorInfo});
        }

        public void RemoveSeries(SensorInfo sensorInfo)
        {
            throw new NotImplementedException();
        }
    }
}
