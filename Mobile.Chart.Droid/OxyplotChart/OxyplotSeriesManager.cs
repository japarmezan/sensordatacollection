using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cloud.Services.DataEntities.SensorEntry;
using Cloud.Services.DataEntities.Utils;
using Mobile.Chart.Droid.OxyplotChart.SeriesContainers;
using Mobile.Chart.Droid.SeriesDefinitions;
using Mobile.Chart.Droid.Services;
using Mobile.Chart.Droid.Utils;
using MoreLinq;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Mobile.Chart.Droid.OxyplotChart
{
    class OxyplotSeriesManager
    {
        private readonly PlotModel Chart;

        private readonly Activity ChartActivity;

        private readonly OxyplotChartRenderer ChartRenderer;

        private readonly AccessTokenDataService dataService;

        private TimeSpan PreviousVisibleTimespan = new TimeSpan(0);

        private bool axisChangedEventSemaphore;

        private double previousVisibleMinimum = 0;

        private double previousVisibleMaximum = 0;

        private LoadedGranularitiesManager loadedGranularitiesManager;

        private DateTime AbsoluteMaximumLastUpdate = DateTime.UtcNow;
        
        internal IList<UserSeriesDefinition> UserSeries;

        internal IDictionary<string, LevelContainer> SeriesContainer;
        
        internal DateTime AbsoluteMaximum;

        internal DateTime AbsoluteMinimum;

        
        public LoadTimespansDefinition LoadTimespansDefinition { get; set; }
        public ZoomThresholdDefinition ZoomThresholdDefinition { get; set; }

        public string CurrentGranularity { get; set; } = Granularity.CoreValue;

        public string CurrentLabelFormat { get; set; } = "dd/MM/yy HH:mm:ss";

        public LinearAxis VerticalAxis { get; }
        public DateTimeAxis HorizontalAxis { get; }


        public OxyplotSeriesManager(OxyplotChartRenderer chartRenderer, PlotModel chart, Activity activity)
        {
            Chart = chart;
            ChartActivity = activity;
            ChartRenderer = chartRenderer;

            VerticalAxis = (LinearAxis)Chart.Axes[0];
            HorizontalAxis = (DateTimeAxis)Chart.Axes[1];

            dataService = new AccessTokenDataService();

            UserSeries = new List<UserSeriesDefinition>();
            SeriesContainer = new Dictionary<string, LevelContainer>();

            LoadTimespansDefinition = new LoadTimespansDefinition()
            {
                CoreValueLoadTimeSpan = new TimeSpan(1, 0, 0),
                DayRegularLoadTimeSpan = new TimeSpan(50, 0, 0),
                HourBatchLoadTimeSpan = new TimeSpan(7, 0, 0)
            };

            ZoomThresholdDefinition = new ZoomThresholdDefinition()
            {
                CoreValueZoomThreshold = new TimeSpan(3, 0, 0)
            };

            loadedGranularitiesManager = new LoadedGranularitiesManager(ZoomThresholdDefinition);

            AbsoluteMaximum = new DateTime(2016, 9, 1, 12, 10, 0);  // will be now
            AbsoluteMinimum = new DateTime(2016, 1, 1, 0, 0, 0);

            HorizontalAxis.Maximum = DateTimeAxis.ToDouble(AbsoluteMaximum);
            HorizontalAxis.Minimum = DateTimeAxis.ToDouble(AbsoluteMaximum - LoadTimespansDefinition.GetTimeSpan(CurrentGranularity));
        }
        
        
        public void AddUserSeriesDefinition(UserSeriesDefinition seriesDefinition)
        {
            if (UserSeries.Contains(seriesDefinition))
            {
                throw new ArgumentException(nameof(seriesDefinition) + " already exists.");
            }

            UserSeries.Add(seriesDefinition);

            foreach (var granularity in seriesDefinition.AllowedGranularities)
            {
                // add granularities which are not in dictionary
                LevelContainer level;
                if (!SeriesContainer.ContainsKey(granularity))
                {
                    level = new LevelContainer(granularity);
                    SeriesContainer.Add(granularity, level);
                    loadedGranularitiesManager.LoadedGranularities.Add(granularity);
                }
                else
                {
                    level = SeriesContainer[granularity];
                }

                level.UserSeriesDefinition.Add(seriesDefinition);
            }
        }

        public async void FirstRender()
        {
            // add timespan from now to past
            var timespan = new LoadedTimespan(AbsoluteMaximum - LoadTimespansDefinition.GetTimeSpan(CurrentGranularity), AbsoluteMaximum);

            var progressDialog = ProgressDialog.Show(ChartActivity, "", "Loading...", true);

            await AddNewSeries(timespan);

            progressDialog.Hide();
        }

        public string GetNearestDataPointValue(DateTime fromDateTime)
        {
            if (SeriesContainer.Count == 0)
            {
                return "";
            }

            var value = "";
            try
            {
                var series = SeriesContainer[CurrentGranularity].LoadedSeries.Where(x => x.LoadedTimespan.Start <= fromDateTime && x.LoadedTimespan.End >= fromDateTime).First();
                value = ((LineSeries)series.Series).Points.MinBy(x => Math.Abs(x.X - DateTimeAxis.ToDouble(fromDateTime))).Y.ToString(CultureInfo.CurrentCulture);
            }
            catch (InvalidOperationException e)
            {
                // not in production
                Toast.MakeText(ChartActivity, e.Message, ToastLength.Long).Show();
            }
            catch (ArgumentNullException) { }

            return value;
        }



        // Merges series with some existing timespan
        private async void AddPointsToSeries(LoadedTimespan toTimespan, DateTime toDateTime, bool addToStart)
        {

            var level = SeriesContainer[CurrentGranularity];

            // todo: blbuvzor pridanie timespanu a serie ktore existuju
            
            foreach (var userSeriesDefinition in level.UserSeriesDefinition)
            {
                var series = level.LoadedSeries.FirstOrDefault(x => x.LoadedTimespan.Equals(toTimespan));

                if (series == null)
                {
                    throw new ArgumentNullException(nameof(series));
                }

                LoadedTimespan addTimespan;

                if (addToStart)
                {
                    addTimespan = new LoadedTimespan(toDateTime, toTimespan.Start);
                    toTimespan.Start = toDateTime;
                }
                else
                {
                    addTimespan = new LoadedTimespan(toTimespan.End, toDateTime);
                    toTimespan.End = toDateTime;
                }

                series.LoadedTimespan = toTimespan;
                
                var data = await FetchDataAsync(userSeriesDefinition.SensorInfo, addTimespan);

                UpdateSeries(series.Series, data, addToStart);
                
                Chart.InvalidatePlot(true);
            }

            //progressDialog.Hide();
        }

        // creates completely new series
        private async Task AddNewSeries(LoadedTimespan timespan)
        {
            var level = SeriesContainer[CurrentGranularity];

            // todo:  co s timespanom co neexistuje
            if (level.LoadedTimespans.Contains(timespan))
            {
                throw new ArgumentException("He was trying to add existing timespan!");
            }

            level.LoadedTimespans.Add(timespan);

            foreach (var userSeriesDefinition in level.UserSeriesDefinition)
            {
                var data = await FetchDataAsync(userSeriesDefinition.SensorInfo, timespan);
                var series = CreateSeries(data, userSeriesDefinition);

                var loadedSeries = new LoadedSeries
                {
                    SeriesDefinition = userSeriesDefinition,
                    LoadedTimespan = timespan,
                    Series = series,
                    DataPointsNumber = data.Count,
                    ServerGranularity = CurrentGranularity
                };

                Chart.Series.Add(series);
                level.LoadedSeries.Add(loadedSeries);

                Chart.InvalidatePlot(true);
            }
            
        }

        private Series CreateSeries(IList<DataPoint> data, UserSeriesDefinition definition)
        {
            Random rnd = new Random();

            var series = new LineSeries
            {
                Color = OxyColor.FromRgb(53, 132, 211),
                //Color = OxyColor.FromRgb(Convert.ToByte(rnd.Next(0, 255)), Convert.ToByte(rnd.Next(0, 255)),
                //        Convert.ToByte(rnd.Next(0, 255))),
                MarkerType = MarkerType.None,
                MarkerSize = 3,
                MarkerFill = OxyColors.DarkSlateGray,
                MarkerStrokeThickness = 0.8,
                MarkerStroke = OxyColors.White,
                StrokeThickness = 2,
                SelectionMode = SelectionMode.Single
            };
            
            series.Points.AddRange(data);
            return series;
        }

        private void UpdateSeries(Series series, IList<DataPoint> data, bool addToStart)
        {
            if (addToStart)
            {
                var newData = new List<DataPoint>(data);
                newData.AddRange(((LineSeries)series).Points);

                ((LineSeries) series).Points.Clear();
                ((LineSeries)series).Points.AddRange(newData);
            }
            else
            {
                ((LineSeries)series).Points.AddRange(data);
            }
            
        }

        
        internal void HandleAxisChange()
        {
            if (SeriesContainer.Count == 0)
            {
                return;
            }

            // set first time absolute minimum and maximum
            if (Math.Abs(HorizontalAxis.AbsoluteMinimum - double.MinValue) < 1)
            {
                UpdateAxisAbsoluteMinimum();
            }

            if (Math.Abs(HorizontalAxis.AbsoluteMaximum - double.MaxValue) < 1)
            {
                UpdateAxisAbsoluteMaximum();
            }

            // set first change in axis
            if (previousVisibleMinimum == 0 || previousVisibleMaximum == 0)
            {
                previousVisibleMinimum = HorizontalAxis.ActualMinimum;
                previousVisibleMaximum = HorizontalAxis.ActualMaximum;
            }

            // check if change is significant
            if (Math.Abs(previousVisibleMinimum - HorizontalAxis.ActualMinimum) < GetVisibleTimespanDouble()/7 &&
                Math.Abs(previousVisibleMaximum - HorizontalAxis.ActualMaximum) < GetVisibleTimespanDouble()/7)
            {
                return;
            }

            // simple semaphore
            if (axisChangedEventSemaphore)
            {
                return;
            }

            // update previous visibility
            previousVisibleMinimum = HorizontalAxis.ActualMinimum;
            previousVisibleMaximum = HorizontalAxis.ActualMaximum;

            axisChangedEventSemaphore = true;

            // visible span
            var currentVisibleTimespan = GetVisibleTimespan();

            // HANDLE CHANGE
            // chart was never zoomed
            if (PreviousVisibleTimespan.Ticks != 0)
            {
                // zoom has actually changed
                if (!currentVisibleTimespan.Equals(PreviousVisibleTimespan))
                {
                    // upadate absolute boundaries
                    UpdateAxisAbsoluteMinimum();
                    UpdateAxisAbsoluteMaximum();

                    // what kind of zoom
                    if (currentVisibleTimespan > PreviousVisibleTimespan)
                    {
                        // zoom OUT event
                        PreviousVisibleTimespan = currentVisibleTimespan;

                        // zoom got over threshold - change series, if higher level exists
                        if (loadedGranularitiesManager.ExistsHigherLevel(CurrentGranularity) && 
                            currentVisibleTimespan > loadedGranularitiesManager.GetThresholdToHigherLevel(CurrentGranularity))
                        {
                            SwitchSeriesVisibility(CurrentGranularity, loadedGranularitiesManager.GetHigherGranularity(CurrentGranularity));
                            CurrentGranularity = loadedGranularitiesManager.GetHigherGranularity(CurrentGranularity);

                            // load missing series
                            if (HandleZoomOverThreshold())
                            {
                                axisChangedEventSemaphore = false;
                                return;
                            }
                        }
                    }
                    else
                    {
                        // zoom IN event
                        PreviousVisibleTimespan = currentVisibleTimespan;

                        // zoom got over threshold - change series
                        if (loadedGranularitiesManager.ExistsLowerLevel(CurrentGranularity) && 
                            currentVisibleTimespan < loadedGranularitiesManager.GetThresholdToLowerLevel(CurrentGranularity))
                        {
                            SwitchSeriesVisibility(CurrentGranularity, loadedGranularitiesManager.GetLowerGranularity(CurrentGranularity));
                            CurrentGranularity = loadedGranularitiesManager.GetLowerGranularity(CurrentGranularity);

                            // load missing series
                            if (HandleZoomOverThreshold())
                            {
                                axisChangedEventSemaphore = false;
                                return;
                            }
                        }
                    }
                }
            }

            PreviousVisibleTimespan = currentVisibleTimespan;

            // scroll left event
            HandleScrollLeft();
            
            // scroll right event
            HandleScrollRight();

            axisChangedEventSemaphore = false;
        }

        private bool HandleZoomOverThreshold()
        {
            // load missing series
            var visibleMinimum = DateTimeAxis.ToDateTime(HorizontalAxis.ActualMinimum) >= AbsoluteMinimum ? 
                DateTimeAxis.ToDateTime(HorizontalAxis.ActualMinimum): AbsoluteMinimum;

            var visibleMaximum = DateTimeAxis.ToDateTime(HorizontalAxis.ActualMaximum) >= AbsoluteMaximum ? 
                DateTimeAxis.ToDateTime(HorizontalAxis.ActualMaximum) : AbsoluteMaximum;

            var minimumIsLoaded = 
                SeriesContainer[CurrentGranularity].LoadedTimespans.Any(x => x.Contains(visibleMinimum));
            var maximumIsLoaded = 
                SeriesContainer[CurrentGranularity].LoadedTimespans.Any(x => x.Contains(visibleMaximum));


            // edges covered by some series
            if (minimumIsLoaded && maximumIsLoaded)
            {
                // if series are the same
                if (SeriesContainer[CurrentGranularity].LoadedTimespans.Any(x => x.Contains(visibleMinimum) && x.Contains(visibleMaximum)))
                {
                    return true;
                }

                // series are not the same - there is a hole in the middle to fill
                var start = SeriesContainer[CurrentGranularity].LoadedTimespans.First(x => x.Contains(visibleMinimum)).End;
                
                var timespanToMerge = SeriesContainer[CurrentGranularity].LoadedTimespans.First(x => x.Contains(visibleMaximum));
                
                AddPointsToSeries(timespanToMerge, start, true);
            }

            // not covered by any series
            if (!minimumIsLoaded && !maximumIsLoaded)
            {
                // find middle
                var middleDate = DateTimeAxis.ToDateTime(HorizontalAxis.ActualMinimum + (HorizontalAxis.ActualMaximum - HorizontalAxis.ActualMinimum) / 2);

                var timespanInPast = NearestTimespanInPast(middleDate);
                var timespanInFuture = NearestTimespanInFuture(middleDate);

                // create start and end by definition
                var start = middleDate - LoadTimespansDefinition.GetTimeSpan(CurrentGranularity);
                var end = middleDate + LoadTimespansDefinition.GetTimeSpan(CurrentGranularity);

                // adjust the start
                if (timespanInPast != null && start < timespanInPast.End)
                {
                    start = timespanInPast.End;
                }

                if (timespanInPast == null && start < AbsoluteMinimum)
                {
                    start = AbsoluteMinimum;
                }

                // adjust the end
                if (timespanInFuture != null && end > timespanInFuture.Start)
                {
                    end = timespanInFuture.Start;
                }
                
                if (timespanInFuture == null && end > AbsoluteMaximum)
                {
                    end = AbsoluteMaximum;
                }

                // if meeting with future timespan, merge
                if (timespanInFuture != null && end > timespanInFuture.Start)
                {
                    
                    AddPointsToSeries(timespanInFuture, start, true);
                    return true;
                }

                // if meeting with past timespan, merge
                if (timespanInPast != null && start < timespanInPast.End)
                {
                    AddPointsToSeries(timespanInPast, end, false);
                    return true;
                }

                // if not meeting, create new timespan
                AddNewSeries(new LoadedTimespan(start, end));

                return true;
            }

            // only one edge covered by series - will handle HandleScrollLeft and HandleScrollRight
            return false;
        }

        private void HandleScrollLeft()
        {
            var newVisibleMinimum = DateTimeAxis.ToDateTime(HorizontalAxis.ActualMinimum) >= AbsoluteMinimum ?
                DateTimeAxis.ToDateTime(HorizontalAxis.ActualMinimum) : AbsoluteMinimum;
            
            // current visible minimum is not downloaded
            if (!SeriesContainer[CurrentGranularity].LoadedTimespans.Any(x => x.Contains(newVisibleMinimum)))
            {
                // find this timespan
                var currentTimespan = NearestTimespanInFuture(newVisibleMinimum);

                if (currentTimespan == null)
                {
                    throw new ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException();
                }

                // find closest past timespan or set by definition
                var newTimespanStart = currentTimespan.Start -
                                        LoadTimespansDefinition.GetTimeSpan(CurrentGranularity);

                var nearestTimespanInPast = NearestTimespanInPast(newVisibleMinimum);

                if (nearestTimespanInPast != null && nearestTimespanInPast.End > newTimespanStart)
                {
                    newTimespanStart = nearestTimespanInPast.End;
                }
                else
                {
                    // if window got over absolute minimum
                    if (AbsoluteMinimum > newTimespanStart)
                    {
                        newTimespanStart = AbsoluteMinimum;
                    }
                }
                
                AddPointsToSeries(currentTimespan, newTimespanStart, true);
            }
        }

        private void HandleScrollRight()
        {
            DateTime newVisibleMaximum;

            if (DateTimeAxis.ToDateTime(HorizontalAxis.ActualMaximum) >= AbsoluteMaximum &&
                AbsoluteMaximumLastUpdate.AddHours(1) < DateTime.UtcNow)
            {
                UpdateAbsoluteMaximum();
                newVisibleMaximum = AbsoluteMaximum;
            }
            else
            {
                newVisibleMaximum = DateTimeAxis.ToDateTime(HorizontalAxis.ActualMaximum) >= AbsoluteMaximum ?
                DateTimeAxis.ToDateTime(HorizontalAxis.ActualMaximum) : AbsoluteMaximum;
            }
            
            // current visible maximum is not downloaded
            if (!SeriesContainer[CurrentGranularity].LoadedTimespans.Any(x => x.Contains(newVisibleMaximum)))
            {
                // find this timespan
                var currentTimespan = NearestTimespanInPast(newVisibleMaximum);

                if (currentTimespan == null)
                {
                    throw new ShouldNotEverHappenAndIfThisHappendThenIamInABigTroubleDamnItException();
                }

                // find close future timespan or set by definition
                var newTimespanEnd = currentTimespan.End + LoadTimespansDefinition.GetTimeSpan(CurrentGranularity);

                var nearestTimespanInFuture = NearestTimespanInFuture(newVisibleMaximum);

                if (nearestTimespanInFuture != null && nearestTimespanInFuture.Start < newTimespanEnd)
                {
                    newTimespanEnd = nearestTimespanInFuture.Start;
                }
                else
                {
                    // if window got over current absolute maximum
                    if (AbsoluteMaximum < newTimespanEnd)
                    {
                        newTimespanEnd = AbsoluteMaximum;
                    }
                }
                
                AddPointsToSeries(currentTimespan, newTimespanEnd, false);
            }
        }

        private void UpdateAbsoluteMaximum()
        {
            AbsoluteMaximum = DateTime.UtcNow;
            AbsoluteMaximumLastUpdate = DateTime.UtcNow;

            ChartRenderer.NowLineAnnotation.X = DateTimeAxis.ToDouble(AbsoluteMaximum);
        }

        private void UpdateAxisAbsoluteMinimum()
        {
            HorizontalAxis.AbsoluteMinimum = DateTimeAxis.ToDouble(AbsoluteMinimum) - (HorizontalAxis.ActualMaximum - HorizontalAxis.ActualMinimum - ChartRenderer.GetTrackerOffset());
        }

        private void UpdateAxisAbsoluteMaximum()
        {
            HorizontalAxis.AbsoluteMaximum = DateTimeAxis.ToDouble(AbsoluteMaximum) + ChartRenderer.GetTrackerOffset();
        }

        private void SetAbsoluteMinimum(int taphomeTime)
        {
            AbsoluteMinimum = TapHomeTime.FromTapHomeTime(taphomeTime);
        }

        private DateTime GetAxisAbsoluteMaximum()
        {
            return DateTimeAxis.ToDateTime(HorizontalAxis.AbsoluteMaximum);
        }

        private DateTime GetAxisAbsoluteMinimum()
        {
            return DateTimeAxis.ToDateTime(HorizontalAxis.AbsoluteMinimum);
        }

        private TimeSpan GetVisibleTimespan()
        {
            return DateTimeAxis.ToDateTime(HorizontalAxis.ActualMaximum) - DateTimeAxis.ToDateTime(HorizontalAxis.ActualMinimum);
        }

        private double GetVisibleTimespanDouble()
        {
            return HorizontalAxis.ActualMaximum - HorizontalAxis.ActualMinimum;
        }

        private void SwitchSeriesVisibility(string invisibleGranularity, string visibleGranularity)
        {
            foreach (var series in SeriesContainer[invisibleGranularity].LoadedSeries)
            {
                series.Series.IsVisible = false;
            }

            foreach (var series in SeriesContainer[visibleGranularity].LoadedSeries)
            {
                series.Series.IsVisible = true;
            }
        }

        private LoadedTimespan NearestTimespanInFuture(DateTime fromDateTime)
        {
            var timespansInFuture =
                SeriesContainer[CurrentGranularity].LoadedTimespans.Where(x => x.Start > fromDateTime).ToList();

            if (timespansInFuture.Count == 0)
            {
                return null;
            }

            return timespansInFuture.MinBy(x => x.Start - fromDateTime);
        }

        private LoadedTimespan NearestTimespanInPast(DateTime fromDateTime)
        {
            var timespansInPast =
                SeriesContainer[CurrentGranularity].LoadedTimespans.Where(x => fromDateTime > x.End).ToList();

            if (timespansInPast.Count == 0)
            {
                return null;
            }

            return timespansInPast.MinBy(x => x.End - fromDateTime);
        }
        

        private async Task<IList<DataPoint>> FetchDataAsync(SensorInfo sensorInfo, LoadedTimespan timespan)
        {
            // TODO: Implement caching to mobile storage

            var fetchedData = await dataService.GetRange(sensorInfo.LocationId, sensorInfo.DeviceId, sensorInfo.ValueTypeId, TapHomeTime.ToTapHomeTime(timespan.Start), TapHomeTime.ToTapHomeTime(timespan.End), CurrentGranularity, sensorInfo.ValueFunction);

            var firstEntry = fetchedData.FirstOrDefault(x => x.SessionId == Guid.Empty);

            if (firstEntry != null)
            {
                SetAbsoluteMinimum(firstEntry.TimeStamp);
            }

            var oxyplotData = fetchedData.Select(x => DateTimeAxis.CreateDataPoint(TapHomeTime.FromTapHomeTime(x.TimeStamp), Math.Round(x.Value, 2))).ToList();

            return oxyplotData;
        }

        
    }
}