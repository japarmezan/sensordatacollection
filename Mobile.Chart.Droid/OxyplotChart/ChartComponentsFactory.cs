using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;

namespace Mobile.Chart.Droid.OxyplotChart
{
    public static class ChartComponentsFactory
    {
        internal static PlotModel CreatePlotModel()
        {
            return new PlotModel
            {
                PlotAreaBorderThickness = new OxyThickness(0),
                PlotAreaBorderColor = OxyColors.Transparent
            };
        }

        internal static LinearAxis CreateLinearAxis()
        {
            return new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.None,
                MinorGridlineStyle = LineStyle.None,
                TickStyle = TickStyle.None,
                IsZoomEnabled = false,
                IsPanEnabled = false,
                FontSize = 18,
                TextColor = OxyColors.DarkSlateGray
            };
        }

        internal static DateTimeAxis CreateDateTimeAxis()
        {
            return new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                IntervalType = DateTimeIntervalType.Auto,
                MinorIntervalType = DateTimeIntervalType.Auto,
                IsZoomEnabled = true,
                IsPanEnabled = true,
                TickStyle = TickStyle.None,
                MajorGridlineStyle = LineStyle.Solid,
                MajorGridlineColor = OxyColors.Gainsboro,
                MajorGridlineThickness = 1,
                FontSize = 18,
                TextColor = OxyColors.DarkSlateGray
            };
        }

        internal static LinearAxis CreateLinearAxisSimpleAppearance()
        {
            return new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.None,
                MinorGridlineStyle = LineStyle.None,
                TickStyle = TickStyle.None,
                IsZoomEnabled = false,
                IsPanEnabled = false,
                FontSize = 8,
                TextColor = OxyColors.DarkSlateGray
            };
        }

        internal static DateTimeAxis CreateDateTimeAxisSimpleAppearance()
        {
            return new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                IsZoomEnabled = false,
                IsPanEnabled = false,
                TickStyle = TickStyle.None,
                MajorGridlineStyle = LineStyle.None,
                MinorGridlineStyle = LineStyle.None,
                FontSize = 8,
                //IntervalLength = 140,
                IntervalType = DateTimeIntervalType.Hours,
                TextColor = OxyColors.DarkSlateGray
            };
        }

        internal static LineAnnotation CreateAbsoluteMaximumLine(DateTime absoluteMaximum)
        {
            return new LineAnnotation
            {
                Type = LineAnnotationType.Vertical,
                Color = OxyColors.Pink,
                StrokeThickness = 2,
                ClipByYAxis = true,
                ClipText = true,
                TextOrientation = AnnotationTextOrientation.Horizontal,
                TextHorizontalAlignment = HorizontalAlignment.Left,
                TextPadding = 10,
                TextMargin = 35,
                FontSize = 26,
                TextColor = OxyColors.DarkSlateGray,
                Text = "now",
                X = DateTimeAxis.ToDouble(absoluteMaximum)
            };

        }
        internal static LineAnnotation CreateTrackerLine()
        {
            return new LineAnnotation
            {
                Type = LineAnnotationType.Vertical,
                Color = OxyColors.Orange,
                StrokeThickness = 2,
                ClipByYAxis = true,
                ClipText = true,
                TextOrientation = AnnotationTextOrientation.Horizontal,
                TextHorizontalAlignment = HorizontalAlignment.Left,
                TextPadding = 10,
                FontSize = 26,
                TextColor = OxyColors.DarkSlateGray
            };
        }
    }
}