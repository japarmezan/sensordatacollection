using System;

namespace Mobile.Chart.Droid.OxyplotChart.SeriesContainers
{
    public class LoadedTimespan
    {
        public LoadedTimespan(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public bool Contains(DateTime dateTime)
        {
            return dateTime >= Start && dateTime <= End;
        }

        public override bool Equals(object obj)
        {
            LoadedTimespan l = obj as LoadedTimespan;

            if (l == null)
            {
                return false;
            }

            return Start.Equals(l.Start) && End.Equals(l.End);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Start.GetHashCode());
                hashCode = (hashCode * 397) ^ (End.GetHashCode());
                return hashCode;
            }
        }
    }
}