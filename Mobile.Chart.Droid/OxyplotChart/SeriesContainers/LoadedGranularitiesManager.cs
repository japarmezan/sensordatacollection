using System;
using System.Collections.Generic;
using Cloud.Services.DataEntities.SensorEntry;
using Mobile.Chart.Droid.SeriesDefinitions;

namespace Mobile.Chart.Droid.OxyplotChart.SeriesContainers
{
    public class LoadedGranularitiesManager
    {
        private ZoomThresholdDefinition zoomThresholdDefinition;

        public IList<string> LoadedGranularities;

        public LoadedGranularitiesManager(ZoomThresholdDefinition zoomThresholdDefinition)
        {
            this.zoomThresholdDefinition = zoomThresholdDefinition;
            LoadedGranularities = new List<string>();
        }

        public bool ExistsHigherLevel(string granularity)
        {
            return GetHigherGranularity(granularity) != null;
        }

        public TimeSpan GetThresholdToHigherLevel(string granularity)
        {
            // must find threshold definition, that is exactly one granularity lower then next higher available granularity
            // i.e. available are: value, hour, year - > for hour must find month's ZoomThresholdDefinition
            return zoomThresholdDefinition.GetThreshold(Granularity.GetLowerLevel(GetHigherGranularity(granularity)));
        }

        public string GetHigherGranularity(string granularity)
        {
            string granularityStep = granularity;
            do
            {
                try
                {
                    granularityStep = Granularity.GetHigherLevel(granularityStep);
                }
                catch (ArgumentOutOfRangeException)
                {
                    return null;
                }
            } while (!LoadedGranularities.Contains(granularityStep));

            return granularityStep;
        }

        public bool ExistsLowerLevel(string granularity)
        {
            return GetLowerGranularity(granularity) != null;
        }

        public TimeSpan GetThresholdToLowerLevel(string granularity)
        {
            // must find threshold definition, that is exactly one granularity lower current granularity
            // i.e. available are: value, hour, year - > for year must find month's ZoomThresholdDefinition
            return zoomThresholdDefinition.GetThreshold(Granularity.GetLowerLevel(granularity));
        }

        public string GetLowerGranularity(string granularity)
        {
            string granularityStep;
            do
            {
                try
                {
                    granularityStep = Granularity.GetLowerLevel(granularity);
                }
                catch (ArgumentOutOfRangeException)
                {
                    return null;
                }
            } while (!LoadedGranularities.Contains(granularityStep));

            return granularityStep;
        }
    }
}