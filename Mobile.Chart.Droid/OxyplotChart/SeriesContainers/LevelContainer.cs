using System;
using System.Collections.Generic;
using System.Linq;
using Mobile.Chart.Droid.SeriesDefinitions;
using MoreLinq;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Mobile.Chart.Droid.OxyplotChart.SeriesContainers
{
    class LevelContainer
    {
        public string ReferenceGranularity { get; }

        public IList<UserSeriesDefinition> UserSeriesDefinition;

        // redundant
        public IList<LoadedTimespan> LoadedTimespans;

        public IList<LoadedSeries> LoadedSeries;

        public DateTime MaximumDateTime;

        public LevelContainer(string referenceGranularity)
        {
            ReferenceGranularity = referenceGranularity;
            UserSeriesDefinition = new List<UserSeriesDefinition>();
            LoadedSeries = new List<LoadedSeries>();
            LoadedTimespans = new List<LoadedTimespan>();
        }

        // to be deleted
        public DateTime SetMaximumDateTime()
        {
            if (LoadedSeries.Count == 0)
            {
                throw new ArgumentNullException(nameof(LoadedSeries));
            }

            MaximumDateTime = DateTimeAxis.ToDateTime(((LineSeries) LoadedSeries.MaxBy(x => x.LoadedTimespan.End).Series).Points.Last().X);
            //MaximumDateTime = DateTimeAxis.ToDateTime(((LineSeries)LoadedSeries.First(x => x.LoadedTimespan.Contains())

            return MaximumDateTime;
        }
    }
}