using Mobile.Chart.Droid.SeriesDefinitions;
using OxyPlot.Series;

namespace Mobile.Chart.Droid.OxyplotChart.SeriesContainers
{
    public class LoadedSeries
    {
        // redundant
        public UserSeriesDefinition SeriesDefinition { get; set; }
        
        public LoadedTimespan LoadedTimespan { get; set; }

        // redundant
        public string ServerGranularity { get; set; }

        public int DataPointsNumber { get; set; }
        
        public Series Series { get; set; }
        

        public override bool Equals(object obj)
        {
            LoadedSeries s = obj as LoadedSeries;

            if (s == null)
            {
                return false;
            }

            return SeriesDefinition.Equals(s.SeriesDefinition) && LoadedTimespan.Equals(s.LoadedTimespan) &&
                   Series.Equals(s.Series) && ServerGranularity.Equals(s.ServerGranularity);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = SeriesDefinition?.GetHashCode() ?? 0;
                hashCode = (hashCode * 397) ^ (LoadedTimespan?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Series?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (ServerGranularity?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}