using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Mobile.Chart.Droid
{
    public abstract class ChartRenderer
    {
        protected Activity ChartActivity;
        protected Context ChartContext;

        public void DoWithLoadingDialog(Action action)
        {
            var progressDialog = ProgressDialog.Show(ChartActivity, "", "Loading...", true);

            new Thread(new ThreadStart(delegate
            {
                action();

                //Activity.RunOnUiThread(() => Toast.MakeText(Context, "Data downloaded", ToastLength.Short).Show());
                ChartActivity.RunOnUiThread(() => progressDialog.Hide());

            })).Start();
        }

        public void ManageChart(Action action)
        {
            ChartActivity.RunOnUiThread(action);
        }

    }
}