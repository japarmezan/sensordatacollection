﻿using System;

namespace Cloud.Services.DataEntities.Utils
{

    public class TapHomeTime
    {
        public static DateTime TAP_HOME_EPOCH = new DateTime(2016, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static readonly int SECONDS_FROM_UNIX_EPOCH_TO_TAPHOME_EPOCH = (int)
            (TAP_HOME_EPOCH - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;


        public static int TapHomeNow()
        {
            return (int) (DateTime.Now - TAP_HOME_EPOCH).TotalSeconds;
        }

        public static int ToTapHomeTime(DateTime fromTime)
        {
            return (int)(fromTime - TAP_HOME_EPOCH).TotalSeconds;
        }

        public static DateTime FromTapHomeTime(int fromTime)
        {
            return TAP_HOME_EPOCH.AddSeconds(fromTime);
        }

        public static Int64 TapHomeTimeToUnixMiliseconds(int fromTime)
        {
            return (fromTime + SECONDS_FROM_UNIX_EPOCH_TO_TAPHOME_EPOCH) * 1000;
        }

    }
}
