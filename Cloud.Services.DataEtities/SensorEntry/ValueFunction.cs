﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud.Services.DataEntities.SensorEntry
{
    public class ValueFunction
    {
        public const string Average = "a";

        public const string Minimum = "n";

        public const string Maximum = "x";

        public static bool IsValidCode(string name)
        {
            return name.Equals(Average) || name.Equals(Minimum) || name.Equals(Maximum);
        }

        public new static string ToString()
        {
            return string.Join(", ", Average, Minimum, Maximum);
        }
    }
}
