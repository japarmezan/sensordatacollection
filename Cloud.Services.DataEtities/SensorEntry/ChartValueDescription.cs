﻿namespace Cloud.Services.DataEntities.SensorEntry
{
    public class ChartValueDescription
    {
        public ChartType ChartType { get; set; }

        public EntryValueType EntryValueType { get; set; }
        
    }


    public enum ChartType
    {
        LineChart,
        BarChart,
        DigitalChart    // on and off chart
    }

    public enum EntryValueType
    {
        // -inf to +inf
        Analog,
        // 0 to 100
        BoundedAnalog,
        // 0 or 1
        Digital
    }

   
    public enum RecordingType
    {
        Periodically,
        OnChange
    }

    // table storage - partition key - LocationId
    // table storage - row key - DeviceId_ValueTypeId_RecordedOn
}
