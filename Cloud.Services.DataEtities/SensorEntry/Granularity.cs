﻿using System;

namespace Cloud.Services.DataEntities.SensorEntry
{
    public static class Granularity
    {
        public const string CoreValue = "v";

        public const string CoreHourBatch = "b";

        public const string DayRegular = "d";

        public const string WeekRegular = "w";

        public const string MonthRegular = "m";

        public const string YearRegular = "y";

        public static string GetHigherLevel(string granularity)
        {
            switch (granularity)
            {
                case CoreValue:
                    return CoreHourBatch;
                case CoreHourBatch:
                    return DayRegular;
                case DayRegular:
                    return WeekRegular;
                case WeekRegular:
                    return MonthRegular;
                case MonthRegular:
                    return YearRegular;
                case YearRegular:
                    //return YearRegular;
                    throw new ArgumentOutOfRangeException();
                default:
                    throw new ArgumentException(granularity);
            }
        }
        public static string GetLowerLevel(string granularity)
        {
            switch (granularity)
            {
                case CoreValue:
                    //return CoreValue;
                    throw new ArgumentOutOfRangeException();
                case CoreHourBatch:
                    return CoreValue;
                case DayRegular:
                    return CoreHourBatch;
                case WeekRegular:
                    return DayRegular;
                case MonthRegular:
                    return WeekRegular;
                case YearRegular:
                    return MonthRegular;
                default:
                    throw new ArgumentException(granularity);
            }
        }

        public static string GetCodeFromName(string name)
        {
            switch (name)
            {
                case "Core Value":
                    return CoreValue;
                case "Core Hour Batch":
                    return CoreHourBatch ;
                case "Day Regular":
                    return DayRegular ;
                case "Week Regular":
                    return WeekRegular ;
                case "Month Regular":
                    return MonthRegular ;
                case "Year Regular":
                    return YearRegular;
                default:
                    return null;
            }
        }

        public static bool IsValidCode(string name)
        {
            return name.Equals(CoreValue) || name.Equals(CoreHourBatch) || name.Equals(DayRegular) ||
                   name.Equals(WeekRegular) || name.Equals(MonthRegular) || name.Equals(YearRegular);
        }

        public new static string ToString()
        {
            return string.Join(", ", CoreValue, CoreHourBatch, DayRegular, WeekRegular, MonthRegular, YearRegular);
        }
    }
}