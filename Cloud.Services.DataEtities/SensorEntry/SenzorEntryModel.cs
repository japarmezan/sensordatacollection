﻿using System;

namespace Cloud.Services.DataEntities.SensorEntry
{
    public class SenzorEntryModel
    {
        public SenzorEntryModel()
        {
        }

        public SenzorEntryModel(SenzorEntryModel model, int timestamp, double value, string granularity, string valueFunction)
        {
            ChartValueDescription = model.ChartValueDescription;
            LocationId = model.LocationId;
            DeviceId = model.DeviceId;
            ValueTypeId = model.ValueTypeId;
            SessionId = model.SessionId;
            TimeStamp = timestamp;
            Value = value;
            Granularity = granularity;
            ValueFunction = valueFunction;
        }

        public SenzorEntryModel(ChartValueDescription chartValueDescription, Guid locationId, long deviceId, int valueTypeId, Guid sessionId, string granularity, string valueFunction, int timeStamp, double value)
        {
            ChartValueDescription = chartValueDescription;
            LocationId = locationId;
            DeviceId = deviceId;
            ValueTypeId = valueTypeId;
            SessionId = sessionId;
            TimeStamp = timeStamp;
            Value = value;
            Granularity = granularity;
            ValueFunction = valueFunction;
        }

        public ChartValueDescription ChartValueDescription { get; set; }
        
        public Guid LocationId { get; set; }

        public long DeviceId { get; set; }

        public int ValueTypeId { get; set; }

        public Guid SessionId {get; set; }

        public string Granularity { get; set; }

        public string ValueFunction { get; set; }

        public int TimeStamp { get; set; }

        public double Value { get; set; }

    }
}
