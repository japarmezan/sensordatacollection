# README #
Source codes for my master thesis project.

System generates test data resambling data collected from home automation systems.
Data are processed and stored in NoSQL database.
Android application with OxyPlot charts are used for visualisation.
