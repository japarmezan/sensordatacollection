﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloud.Services.DataEntities.SensorEntry;
using Core.StreamDataProcessor.Filter;

namespace Core.StreamDataProcessor.Processor
{
    public class StreamProcessor : IStreamProcessor
    {
        private readonly IStreamFilter _filter;

        public StreamProcessor(IStreamFilter filter)
        {
            // TODO: DI
            this._filter = filter;
        }

        public IList<SenzorEntryModel> BatchProcessData(IDictionary<int, float> data, Guid sessionId, Guid locationId, long deviceId, int valueTypeId, ChartValueDescription chartValueDescription, FilterSettings filterSettings)
        {
            IList<SenzorEntryModel> batchedData = (List<SenzorEntryModel>)data.Select(x => new SenzorEntryModel(chartValueDescription, locationId, deviceId, valueTypeId, sessionId, Granularity.CoreValue, ValueFunction.Average, x.Key, x.Value)).ToList();
            IList<SenzorEntryModel> filteredData = _filter.FilterAnalogData(batchedData, filterSettings);

            return filteredData;

        }
    }
}
