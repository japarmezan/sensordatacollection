﻿using System;
using System.Collections.Generic;
using Cloud.Services.DataEntities.SensorEntry;
using Core.StreamDataProcessor.Filter;

namespace Core.StreamDataProcessor.Processor
{
    public interface IStreamProcessor
    {
        IList<SenzorEntryModel> BatchProcessData(IDictionary<int, float> data, Guid sessionId, Guid locationId, long deviceId,
            int valueTypeId, ChartValueDescription chartValueDescription, FilterSettings filterSettings);
    }
}
