﻿using System.Collections.Generic;
using Cloud.Services.DataEntities.SensorEntry;

namespace Core.StreamDataProcessor.Filter
{
    public interface IStreamFilter
    {
        IList<SenzorEntryModel> FilterAnalogData(IList<SenzorEntryModel> data, FilterSettings filterSettings);

        IDictionary<int, float> FilterAnalogData(IDictionary<int, float> data, FilterSettings filterSettings);

        IList<SenzorEntryModel> FilterDigitalData(IList<SenzorEntryModel> data);
        
    }
}
