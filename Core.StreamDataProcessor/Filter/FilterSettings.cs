﻿namespace Core.StreamDataProcessor.Filter
{
    public enum FilterType
    {
        AnalogDataFilter,
        DigitalDataFilter
    }


    public class FilterSettings
    {
        public FilterSettings(FilterType filterType, double minChangeToDetect)
        {
            FilterType = filterType;
            MinChangeToDetect = minChangeToDetect;
        }

        public FilterType FilterType { get; set; }

        // percentual value of change
        public double MinChangeToDetect { get; set; }

        // number of entries in timespan
        public double MaxEntriesInTimespan { get; set; }

        // timespan for max number of entries
        public int TimespanInSeconds { get; set; }


        // ...plus other needed filter settings 
    }
}
